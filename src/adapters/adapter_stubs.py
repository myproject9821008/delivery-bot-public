

class AdapterStubs:
    @staticmethod
    async def __apply_common_filter(data: dict, common_filter: str) -> dict:
        """Returns filtered data"""
        result = {}
        if common_filter == 'sub':
            for key, val in data.items():
                if val['sub']:
                    result[key] = val
            return result

        if common_filter == 'fav':
            for key, val in data.items():
                if val['favorite']:
                    result[key] = val
            return result

        return data

    @staticmethod
    async def __apply_custom_filter(data: dict, custom_filter: str) -> dict:
        """Top filter just returns 1/2 data for example"""

        if custom_filter == '':
            return data

        if custom_filter == 'top':
            stop_count = len(data) // 2
            result = {}
            counter = 0
            for key, value in data.items():
                result[key] = value
                counter += 1
                if counter == stop_count:
                    break
            return result

        result = {}
        for key, val in data.items():
            search_target = custom_filter.lower()
            item_name = val['name'].lower()
            if search_target in item_name:
                result[key] = val

        return result

    @staticmethod
    async def __off_all_state(data: dict) -> dict:
        for key, value in data.items():
            data[key]['sub'] = False
        return data

    @staticmethod
    async def __paginate_data(count: int, offset: int, data: dict) -> dict:
        total_count = len(data)
        selection = {}
        counter = 0
        for key, value in data.items():
            counter += 1
            if counter <= offset:
                continue
            if counter <= count + offset:
                selection[key] = value
        result = {'total_count': total_count, 'selection': selection}
        return result

    @staticmethod
    async def __check_uniform_condition(data: dict) -> bool:
        """Tests all conditions as True or all conditions as False"""
        first_val = None
        for ind, val in enumerate(data.values()):
            if ind == 0:
                first_val = val['sub']
                continue
            if val['sub'] is not first_val:
                return False
        return True

    async def __apply_filters(self, data: dict, custom_filter: str, common_filter: str) -> dict:
        with_top = await self.__apply_custom_filter(data, custom_filter)
        with_filter = await self.__apply_common_filter(with_top, common_filter) if common_filter else with_top
        return with_filter


channels = {
    '123456': {'name': 'Your Best Coin', 'sub': False, 'favorite': False},
    '123457': {'name': 'Too many Coins', 'sub': True, 'favorite': True},
    '123458': {'name': 'Only Crypt', 'sub': False, 'favorite': True},
    '123459': {'name': 'Pss... guy!', 'sub': True, 'favorite': True},
    '123460': {'name': 'Pay for lucky day', 'sub': False, 'favorite': False},
    '123461': {'name': 'Your Best Coin 1', 'sub': False, 'favorite': True},
    '123462': {'name': 'Too many Coins 1', 'sub': True, 'favorite': False},
    '123463': {'name': 'Only Crypt 1', 'sub': False, 'favorite': False},
    '123464': {'name': 'Pss... guy! 1', 'sub': True, 'favorite': True},
    '123465': {'name': 'Pay for lucky day 1', 'sub': False, 'favorite': False},
    '123466': {'name': 'Your Best Coin 2', 'sub': False, 'favorite': False},
    '123467': {'name': 'Too many Coins 2', 'sub': True, 'favorite': True},
    '123468': {'name': 'Only Crypt 2', 'sub': False, 'favorite': True},
    '123469': {'name': 'Pss... guy! 2', 'sub': True, 'favorite': True},
    '123470': {'name': 'Pay for lucky day 2', 'sub': False, 'favorite': True},
    '123471': {'name': 'Your Best Coin 3', 'sub': False, 'favorite': False},
    '123472': {'name': 'Too many Coins 3', 'sub': True, 'favorite': False},
    '123473': {'name': 'Only Crypt 3', 'sub': False, 'favorite': True},
    '123474': {'name': 'Pss... guy! 3', 'sub': True, 'favorite': True},
    '123475': {'name': 'Pay for lucky day 3', 'sub': False, 'favorite': False},
}

chats = {
    '123456': {'name': 'Your Best Chat', 'sub': False, 'favorite': False},
    '123457': {'name': 'Too many Chats', 'sub': True, 'favorite': True},
    '123458': {'name': 'Only Chat', 'sub': False, 'favorite': False},
    '123459': {'name': 'Pss... Chat!', 'sub': True, 'favorite': True},
    '123460': {'name': 'Pay for lucky Chat', 'sub': False, 'favorite': True},
    '123461': {'name': 'Your Best Chat 1', 'sub': False, 'favorite': True},
    '123462': {'name': 'Too many Chat 1', 'sub': True, 'favorite': False},
    '123463': {'name': 'Only Chat 1', 'sub': False, 'favorite': True},
    '123464': {'name': 'Pss... Chat! 1', 'sub': True, 'favorite': True},
    '123465': {'name': 'Pay for lucky Chat 1', 'sub': False, 'favorite': False},
    '123466': {'name': 'Your Best Chat 2', 'sub': False, 'favorite': True},
    '123467': {'name': 'Too many Chat 2', 'sub': True, 'favorite': True},
    '123468': {'name': 'Only Chat 2', 'sub': False, 'favorite': False},
    '123469': {'name': 'Pss... Chat! 2', 'sub': True, 'favorite': False},
    '123470': {'name': 'Pay for lucky Chat 2', 'sub': False, 'favorite': True},
    '123471': {'name': 'Your Best Chat 3', 'sub': False, 'favorite': True},
    '123472': {'name': 'Too many Chat 3', 'sub': True, 'favorite': False},
    '123473': {'name': 'Only Chat 3', 'sub': False, 'favorite': False},
    '123474': {'name': 'Pss... Chat! 3', 'sub': True, 'favorite': True},
    '123475': {'name': 'Pay for lucky Chat 3', 'sub': False, 'favorite': False},
}

members = {
    '123456': {'name': 'John', 'sub': False, 'favorite': True},
    '123457': {'name': 'Dasdraperma', 'sub': True, 'favorite': False},
    '123458': {'name': 'Mamkin_investor', 'sub': False, 'favorite': True},
    '123459': {'name': 'Ivan!', 'sub': True, 'favorite': False},
    '123460': {'name': 'Paul', 'sub': False, 'favorite': True},
    '123461': {'name': 'Erika', 'sub': False, 'favorite': False},
    '123462': {'name': 'RoboJob', 'sub': True, 'favorite': False},
    '123463': {'name': 'Volandemort', 'sub': False, 'favorite': True},
    '123464': {'name': 'CryptoGuy', 'sub': True, 'favorite': True},
    '123465': {'name': 'AnyGay', 'sub': False, 'favorite': False},
    '123466': {'name': 'Sandra', 'sub': False, 'favorite': True},
    '123467': {'name': 'Pikachu', 'sub': True, 'favorite': False},
    '123468': {'name': 'Cucumber1111', 'sub': False, 'favorite': False},
    '123469': {'name': 'Rick', 'sub': True, 'favorite': True},
    '123470': {'name': 'Julia', 'sub': False, 'favorite': True},
    '123471': {'name': 'Desintegrador3000', 'sub': False, 'favorite': False},
    '123472': {'name': 'Sara', 'sub': True, 'favorite': False},
    '123473': {'name': 'Damon', 'sub': False, 'favorite': True},
    '123474': {'name': 'EasyPeasy', 'sub': True, 'favorite': False},
    '123475': {'name': 'Morty', 'sub': False, 'favorite': True},
}

coins = {
    '123456': {'name': 'BTC', 'sub': False},
    '123457': {'name': 'ETH', 'sub': True},
    '123458': {'name': 'BNB', 'sub': False},
    '123459': {'name': 'ADA', 'sub': True},
    '123460': {'name': 'SOL', 'sub': False},
    '123461': {'name': 'XRP', 'sub': False},
    '123462': {'name': 'DOT', 'sub': True},
    '123463': {'name': 'DOGE', 'sub': False},
    '123464': {'name': 'LINK', 'sub': True},
    '123465': {'name': 'LTC', 'sub': False},
    '123466': {'name': 'AVAX', 'sub': False},
    '123467': {'name': 'UNI', 'sub': True},
    '123468': {'name': 'ATOM', 'sub': False},
    '123469': {'name': 'FIL', 'sub': True},
    '123470': {'name': 'XTZ', 'sub': False},
    '123471': {'name': 'AAVE', 'sub': False},
    '123472': {'name': 'SUSHI', 'sub': True},
    '123473': {'name': 'COMP', 'sub': False},
    '123474': {'name': 'MKR', 'sub': True},
    '123475': {'name': 'ALGO', 'sub': False},
}

chat_doc_example = 'Ссылка: https://www.google.com/\n' \
                   'Участников: 3678\n' \
                   'Уже отслеживают: 695'

member_doc_example = 'Пишет в чатах: 16\n' \
                     'Уже отслеживают: 306'
