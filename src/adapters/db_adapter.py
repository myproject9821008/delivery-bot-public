from entities.menu.channels import ChannelsMenuResponseData, ChannelDetailResponseData
from entities.menu.chats import ChatsMenuResponseData, ChatDetailResponseData, ChatMembersResponseData
from entities.menu.members import MembersMenuResponseData, MemberChatsResponseData
from entities.menu.members.member_details import MemberDetailResponseData
from interfaces.data_adapter_interface import DataAdapterInterface
from entities import menu as menu_entities
from adapter_stubs import *
from schemas.handler_schemas import ChannelsSchema, ChanConfSchema, ChatsSchema, ChatConfSchema, ChMembersSchema, \
    MembersSchema, MemberConfSchema, MemberChSchema


class Menu:
    """Сокращение импортов для использования dto в адаптере"""
    chats = menu_entities.chats
    chat_details = menu_entities.chat_details
    chat_members = menu_entities.chat_members

    channels = menu_entities.channels
    channel_details = menu_entities.channel_details

    members = menu_entities.members
    member_details = menu_entities.member_details
    member_chats = menu_entities.member_chats


class DataAdapter(DataAdapterInterface, AdapterStubs):
    """В режиме разработки адаптер возвращает данные в произвольной форме, не используя dto"""
    def __init__(self, data_source):
        if not data_source:
            raise ValueError(f'Expected database session or object to receive data')
        self.__data_source = data_source

    @property
    def data_source(self):
        """ Must be used with production database"""
        return self.__data_source

    async def get_channels_listing(self, frame: ChannelsSchema) -> ChannelsMenuResponseData:
        user_id = frame.sender
        with_filters = await self.__apply_filters(channels, frame.custom_filter, frame.common_filter)
        result = await self.__paginate_data(frame.count, frame.offset, with_filters)
        return result

    async def change_channels_listing(self, frame: ChannelsSchema) -> ChannelsMenuResponseData:
        user_id = frame.sender
        channel_id = frame.action
        channels[channel_id]['sub'] = not channels[channel_id]['sub']
        with_filters = await self.__apply_filters(channels, frame.custom_filter, frame.common_filter)
        result = await self.__paginate_data(frame.count, frame.offset, with_filters)
        return result

    async def get_one_channel(self, frame: ChanConfSchema) -> ChannelDetailResponseData:
        user_id = frame.sender
        channel_ident = frame.parent.action
        result = channels[channel_ident]
        result['description'] = chat_doc_example
        return channels[channel_ident]

    async def change_one_channel(self, frame: ChanConfSchema) -> ChannelDetailResponseData:
        user_id = frame.sender
        channel_ident = frame.parent.action
        action = frame.action
        if action == 'sub':
            channels[channel_ident]['sub'] = not channels[channel_ident]['sub']
        if action == 'fav':
            channels[channel_ident]['favorite'] = not channels[channel_ident]['favorite']
        return channels[channel_ident]

    # todo ЧАТЫ
    async def get_chats_listing(self, frame: ChatsSchema) -> ChatsMenuResponseData:
        user_id = frame.sender
        with_filters = await self.__apply_filters(chats, frame.custom_filter, frame.common_filter)
        result = await self.__paginate_data(frame.count, frame.offset, with_filters)
        return result

    async def change_chats_listing(self, frame: ChatsSchema) -> ChatsMenuResponseData:
        user_id = frame.sender
        chat_id = frame.action
        chats[chat_id]['sub'] = not chats[chat_id]['sub']
        with_filters = await self.__apply_filters(chats, frame.custom_filter, frame.common_filter)
        result = await self.__paginate_data(frame.count, frame.offset, with_filters)
        return result

    async def get_one_chat(self, frame: ChatConfSchema) -> ChatDetailResponseData:
        user_id = frame.sender
        chat_id = frame.parent.action
        result = chats[chat_id]
        result['description'] = chat_doc_example
        return chats[chat_id]

    async def change_one_chat(self, frame: ChatConfSchema) -> ChatDetailResponseData:
        user_id = frame.sender
        chat_ident = frame.parent.action
        action = frame.action
        if action == 'sub':
            chats[chat_ident]['sub'] = not chats[chat_ident]['sub']
        if action == 'fav':
            chats[chat_ident]['favorite'] = not chats[chat_ident]['favorite']
        return chats[chat_ident]

    # todo ЧАТ - УЧАСТНИКИ
    async def get_chat_members_listing(self, frame: ChMembersSchema) -> ChatMembersResponseData:
        user_id = frame.sender
        chat_id = frame.parent.action
        with_filters = await self.__apply_filters(members, frame.custom_filter, frame.common_filter)
        result = await self.__paginate_data(frame.count, frame.offset, with_filters)
        result['chat_name'] = 'Some chat'
        return result

    async def change_chat_members_listing(self, frame: ChMembersSchema) -> ChatMembersResponseData:
        chat_id = frame.parent.action
        member_id = frame.action
        members[member_id]['sub'] = not members[member_id]['sub']
        with_filters = await self.__apply_filters(members, frame.custom_filter, frame.common_filter)
        result = await self.__paginate_data(frame.count, frame.offset, with_filters)
        result['chat_name'] = 'Some chat'
        return result

    async def off_all_chat_members(self, frame: ChMembersSchema) -> ChatMembersResponseData:
        chat_id = frame.parent.action
        post_members = await self.__off_all_state(members)
        with_filters = await self.__apply_filters(post_members, frame.custom_filter, frame.common_filter)
        result = await self.__paginate_data(frame.count, frame.offset, with_filters)
        result['chat_name'] = 'Some chat'
        return result

    # todo ЛЮДИ
    async def get_members_listing(self, frame: MembersSchema) -> MembersMenuResponseData:
        user_id = frame.sender
        with_filters = await self.__apply_filters(members, frame.custom_filter, frame.common_filter)
        result = await self.__paginate_data(frame.count, frame.offset, with_filters)
        return result

    async def change_members_listing(self, frame: MembersSchema) -> MembersMenuResponseData:
        user_id = frame.sender
        member_id = frame.action
        members[member_id]['sub'] = not members[member_id]['sub']
        with_filters = await self.__apply_filters(members, frame.custom_filter, frame.common_filter)
        result = await self.__paginate_data(frame.count, frame.offset, with_filters)
        return result

    async def get_one_member(self, frame: MemberConfSchema) -> MemberDetailResponseData:
        user_id = frame.sender
        chat_ident = frame.parent.action
        result = members[chat_ident]
        result['description'] = member_doc_example
        return members[chat_ident]

    async def change_one_member(self, frame: MemberConfSchema) -> MemberDetailResponseData:
        user_id = frame.sender
        member_id = frame.parent.action
        action = frame.action
        if action == 'sub':
            members[member_id]['sub'] = not members[member_id]['sub']
        if action == 'fav':
            members[member_id]['favorite'] = not members[member_id]['favorite']
        return members[member_id]

    # todo ЛЮДИ - ЧАТЫ
    async def get_member_chats(self, frame: MemberChSchema) -> MemberChatsResponseData:
        user_id = frame.sender
        member_id = frame.parent.action
        with_filters = await self.__apply_filters(chats, frame.custom_filter, frame.common_filter)
        result = await self.__paginate_data(frame.count, frame.offset, with_filters)
        result['member_name'] = 'Victor Example'
        return result

    async def change_member_chats_listing(self, frame: MemberChSchema) -> MemberChatsResponseData:
        user_id = frame.sender
        chat_id = frame.parent.action
        chats[frame.action]['sub'] = not chats[frame.action]['sub']
        with_filters = await self.__apply_filters(chats, frame.custom_filter, frame.common_filter)
        result = await self.__paginate_data(frame.count, frame.offset, with_filters)
        result['member_name'] = 'Victor Example'
        return result

    async def off_all_member_chats(self, frame: MemberChSchema) -> MemberChatsResponseData:
        user_id = frame.sender
        member_id = frame.parent.action
        post_members = await self.__off_all_state(chats)
        with_filters = await self.__apply_filters(post_members, frame.custom_filter, frame.common_filter)
        result = await self.__paginate_data(frame.count, frame.offset, with_filters)
        result['member_name'] = 'Victor Example'
        return result

    # todo Проверка подписки
    async def check_subscribe(self, user_id: int) -> bool:
        return True
