import os
from pathlib import Path

from dotenv import load_dotenv, find_dotenv

ENVIRONMENT = os.getenv('ENVIRONMENT', 'dev')

if ENVIRONMENT == 'dev':
    load_dotenv(find_dotenv(f'.env.{ENVIRONMENT}'))

DEBUG = bool(os.getenv('DEBUG', None))

BASE_DIR = Path(__file__).resolve().parent
MEDIA_DIR = BASE_DIR.joinpath('media')
FILE_CASH_DIR = BASE_DIR.joinpath('files', 'file_cache.json')

BOT_TOKEN = os.getenv('BOT_TOKEN', None)
API_ID = int(os.getenv('API_ID', None))
API_HASH = os.getenv('API_HASH', None)

ITEMS_PER_PAGE = int(os.getenv('ITEMS_PER_PAGE', 3))
PAG_SKIP_BUTTONS = bool(os.getenv('PAG_SKIP_BUTTONS', False))
PAG_SKIP_COUNT = int(os.getenv('PAG_SKIP_COUNT', 3))
MAX_SEARCH_LENGTH = int(os.getenv('MAX_SEARCH_LENGTH', 15))

SUBSCRIBE_LINK = os.getenv('SUBSCRIBE_LINK', 'https://www.google.com/')

DEFAULT_IMAGE = os.getenv('DEFAULT_IMAGE', None)
CHAT_TO_DOWNLOAD = int(os.getenv('CHAT_TO_DOWNLOAD', None))

GLOBAL_TIME_RANGE_SEC = int(os.getenv('GLOBAL_TIME_RANGE_SEC', 1))
GLOBAL_TASK_COUNT = int(os.getenv('GLOBAL_TASK_COUNT', 28))

STACK_FIRST_TIME_RANGE = int(os.getenv('STACK_FIRST_TIME_RANGE', 60))
STACK_FIRST_TASK_COUNT = int(os.getenv('STACK_FIRST_TASK_COUNT', 18))
STACK_SECOND_TIME_RANGE = int(os.getenv('STACK_SECOND_TIME_RANGE', 1))
STACK_SECOND_TASK_COUNT = int(os.getenv('STACK_SECOND_TASK_COUNT', 1))
