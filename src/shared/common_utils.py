from random import choices

from telethon.extensions import markdown
from telethon import types

SYMBOLS = '0123456789abcdefghijklmnopqrstuvwxwzабвгдеёжзийклмнопрстуфхцчшщэьъюя'


def get_file_reference() -> bytes:
    rand_string = ''.join(choices(SYMBOLS, k=16))
    bytestring = rand_string.encode('utf-8')
    return bytestring


def replace_borders(message: str) -> str:
    without_command_border = message.replace('#', '$')
    result = without_command_border.replace('/', '!')
    return result


def place_borders(message: str) -> str:
    with_command_border = message.replace('$', '#')
    result = with_command_border.replace('!', '/')
    return result


class CustomMarkdown:
    @staticmethod
    def parse(text: str) -> tuple:
        text, entities = markdown.parse(text)
        for i, e in enumerate(entities):
            if isinstance(e, types.MessageEntityTextUrl):
                if e.url == 'spoiler':
                    entities[i] = types.MessageEntitySpoiler(e.offset, e.length)
                elif e.url.startswith('emoji/'):
                    entities[i] = types.MessageEntityCustomEmoji(e.offset, e.length, int(e.url.split('/')[1]))
        return text, entities

    @staticmethod
    def unparse(text: str, entities):
        for i, e in enumerate(entities or []):
            if isinstance(e, types.MessageEntityCustomEmoji):
                entities[i] = types.MessageEntityTextUrl(e.offset, e.length, f'emoji/{e.document_id}')
            if isinstance(e, types.MessageEntitySpoiler):
                entities[i] = types.MessageEntityTextUrl(e.offset, e.length, 'spoiler')
        return markdown.unparse(text, entities)
