import asyncio
from datetime import datetime
from pathlib import Path

from telethon import TelegramClient
from telethon.tl.custom import Message
from telethon.tl.types import InputPhoto, InputDocument

from config import MEDIA_DIR, DEFAULT_IMAGE, FILE_CASH_DIR, CHAT_TO_DOWNLOAD
from interfaces.cache_manager_interface import CacheManagerInterface
from shared.common_utils import get_file_reference
from shared.file_handler import get_hash, get_hashes_and_names, load_json, save_json, remove_file


class CacheManager(CacheManagerInterface):
    def __init__(self, client):
        self.__client = client
        self.__cache = {}

    @property
    def client(self) -> TelegramClient:
        return self.__client

    async def preload(self) -> None:
        current_hashes = get_hashes_and_names(MEDIA_DIR)
        if not current_hashes:
            remove_file(FILE_CASH_DIR)
            return

        cache_storage = load_json(FILE_CASH_DIR)

        to_remove = set(cache_storage.keys()).difference(set(current_hashes.keys()))
        to_update = set(current_hashes.keys()).difference(set(cache_storage.keys()))

        clean_storage = {key: val for key, val in cache_storage.items() if key not in to_remove}
        hashes_to_update = {key: val for key, val in current_hashes.items() if key in to_update}

        if CHAT_TO_DOWNLOAD:
            clean_storage.update(await self.__get_tg_hashes(hashes_to_update))

        save_json(FILE_CASH_DIR, clean_storage)
        self.__cache = {key: self.__pack_tg_hash_to_obj(val) for key, val in clean_storage.items()}

    async def __get_tg_hashes(self, hashes: dict) -> dict:
        for file_hash, file_name in hashes.items():
            any_text = str(datetime.now())
            message = await self.client.send_message(
                entity=CHAT_TO_DOWNLOAD,
                message=any_text,
                file=MEDIA_DIR.joinpath(file_name)
            )
            hashes[file_hash] = self.__extract_tg_hash(message)
            await asyncio.sleep(0.2)
        return hashes

    @staticmethod
    def __extract_tg_hash(message: Message) -> dict:
        if message.photo:
            result = {
                'type': 'input_photo',
                'id': message.photo.id,
                'access_hash': message.photo.access_hash,
            }
            return result

        if message.document:
            result = {
                'type': 'input_document',
                'id': message.document.id,
                'access_hash': message.document.access_hash,
            }
            return result

    @staticmethod
    def __pack_tg_hash_to_obj(filehash: dict) -> InputPhoto | InputDocument:
        if filehash['type'] == 'input_photo':
            file_obj = InputPhoto(
                id=filehash['id'],
                access_hash=filehash['access_hash'],
                file_reference=get_file_reference()
            )
            return file_obj

        if filehash['type'] == 'input_document':
            file_obj = InputDocument(
                id=filehash['id'],
                access_hash=filehash['access_hash'],
                file_reference=get_file_reference()
            )
            return file_obj

    def get_file(self, filename: str = DEFAULT_IMAGE) -> Path | InputPhoto | InputDocument:
        file_hash = get_hash(MEDIA_DIR.joinpath(filename))
        if file_hash in self.__cache:
            return self.__cache[file_hash]
        else:
            return MEDIA_DIR.joinpath(filename)

    def add_file(self, message: Message, filepath: Path) -> None:
        if not isinstance(filepath, Path):
            return

        file_hash = get_hash(filepath)
        hash_to_save = self.__extract_tg_hash(message)

        cache_storage = load_json(FILE_CASH_DIR)
        cache_storage[file_hash] = hash_to_save
        save_json(FILE_CASH_DIR, cache_storage)

        self.__cache[file_hash] = self.__pack_tg_hash_to_obj(hash_to_save)
