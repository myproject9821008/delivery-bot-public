import hashlib
import json
from pathlib import Path
from shutil import rmtree, copy


def copy_file(path_from: Path, path_to: Path, new_name: str = None) -> None:
    """Copying a file"""
    path_to.mkdir(parents=True, exist_ok=True)

    if not new_name:
        full_path_to = path_to.joinpath(path_from.name)
    else:
        full_path_to = path_to.joinpath(new_name)

    copy(path_from, full_path_to, follow_symlinks=True)


def save_json(path: Path, data: dict) -> None:
    """Saving json to file"""
    path.parent.mkdir(parents=True, exist_ok=True)
    with path.open("w", encoding="UTF-8") as target:
        json.dump(data, target, indent=4, ensure_ascii=False)


def load_json(path: Path) -> dict:
    """Loading json from file"""
    try:
        with path.open("r", encoding="UTF-8") as target:
            data = json.load(target)
        return data
    except Exception:
        return {}


def add_string_to_file(path: Path, string: str) -> None:
    """Add string to file"""
    path.parent.mkdir(parents=True, exist_ok=True)
    with path.open('a', encoding="UTF-8") as f:
        f.write(f'\n{string}')


def read_as_lines(path: Path) -> list | None:
    """Loading list of strings from file"""
    try:
        with path.open("r", encoding="UTF-8") as target:
            data = target.readlines()
        return data
    except Exception:
        return None


def remove_file(path: Path) -> None:
    if path.exists() and path.is_file():
        path.unlink()


def remove_folder(path: Path) -> None:
    if path.exists() and path.is_dir():
        rmtree(path)


def create_path(path: Path) -> None:
    path.parent.mkdir(parents=True, exist_ok=True)


def load_string_from_file(path: Path) -> str | None:
    if path.exists() and path.is_file():
        return path.read_text(encoding='utf-8')


def load_bytes_from_file(path: Path) -> bytes | None:
    if path.exists() and path.is_file():
        result = path.read_bytes()
        return result


def save_string_to_file(path: Path, file: str) -> None:
    path.parent.mkdir(parents=True, exist_ok=True)
    path.write_text(file, encoding='utf-8')


def get_hash(path: Path) -> str:
    file_hash = hashlib.md5()
    with path.open('rb') as file:
        data = file.read()
        file_hash.update(data)
    result = file_hash.hexdigest()
    return result


def get_hashes_and_names(path: Path) -> dict:
    hashes = {}
    paths = path.glob('*')
    for single_path in paths:
        if single_path.is_file():
            file_hash = get_hash(single_path)
            filename = single_path.name
            hashes[file_hash] = filename
    return hashes

