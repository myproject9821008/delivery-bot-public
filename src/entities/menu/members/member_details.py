from pydantic import BaseModel


class MemberDetailParams(BaseModel):
    """IN Получить детали по одному мемберу на 2 уровне."""
    user_id: int
    member_id: int


class MemberToggleSubParams(BaseModel):
    """IN Переключить подписку на мембера в противоположное состояние."""
    user_id: int
    member_id: int


class MemberToggleFavParams(BaseModel):
    """IN Переключить избранность мембера в противоположное состояние."""
    user_id: int
    member_id: int


class MemberToggleReactionParams(BaseModel):
    """IN Переключить реакцию у мембера в противоположное состояние."""
    user_id: int
    member_id: int


class MemberDetailResponseData(BaseModel):
    """OUT Целевые данные для отрисовки деталки по одному мемберу на 2 уровне."""
    id: int
    name: str
    is_sub: bool
    is_fav: bool
    in_chats: int | None = None
    tracked_count: int
    likes: int
    dislikes: int
    user_reaction: bool | None = None