from pydantic import BaseModel
from config import ITEMS_PER_PAGE


class MembersMenuParams(BaseModel):
    """IN Получить список мемберов на 1 уровне."""
    user_id: int
    is_top: bool = False
    is_sub: bool = False
    is_fav: bool = False
    search_request: str | None = None
    count: int = ITEMS_PER_PAGE
    offset: int = 0


class MembersMenuToggleSubParams(BaseModel):
    """IN Переключить подписку на мембера в противоположное состояние."""
    user_id: int
    member_id: int
    is_top: bool = False
    is_sub: bool = False
    is_fav: bool = False
    search_request: str | None = None
    count: int = ITEMS_PER_PAGE
    offset: int = 0


class MembersMenuResponseData(BaseModel):
    """OUT Целевые данные для отрисовки списка мемберов на 1 уровне."""

    class MemberItem(BaseModel):
        id: int
        name: str
        is_sub: bool
        is_fav: bool

    total_count: int
    members: list[MemberItem] = []
