from pydantic import BaseModel
from config import ITEMS_PER_PAGE


class MemberChatsParams(BaseModel):
    """IN Получить список чатов мембера на 3 уровне."""
    user_id: int
    member_id: int
    is_sub: bool = False
    search_request: str | None = None
    count: int = ITEMS_PER_PAGE
    offset: int = 0


class MemberChatToggleSubParams(BaseModel):
    """IN Переключить подписку на чаты мембера в противоположное состояние."""
    user_id: int
    chat_id: int
    member_id: int
    is_sub: bool = False
    search_request: str | None = None
    count: int = ITEMS_PER_PAGE
    offset: int = 0


class MemberChatsOffAllSubParams(BaseModel):
    """IN Выключить подписку всех чатов мембера."""
    user_id: int
    member_id: int
    is_sub: bool = False
    search_request: str | None = None
    count: int = ITEMS_PER_PAGE
    offset: int = 0


class MemberChatsResponseData(BaseModel):
    """OUT Целевые данные для отрисовки списка чатов мембера на 3 уровне."""

    class MemberChatItem(BaseModel):
        id: int
        name: str
        is_sub: bool

    total_count: int
    member_name: str
    chats: list[MemberChatItem] = []
