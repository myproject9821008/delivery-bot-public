from pydantic import BaseModel
from config import ITEMS_PER_PAGE


class ChannelsMenuParams(BaseModel):
    """IN Получить список каналов на 1 уровне."""
    user_id: int
    is_top: bool = False
    is_sub: bool = False
    is_fav: bool = False
    search_request: str | None = None
    count: int = ITEMS_PER_PAGE
    offset: int = 0


class ChannelsMenuToggleSubParams(BaseModel):
    """IN Переключить подписку на канал в противоположное состояние."""
    user_id: int
    channel_id: int
    is_top: bool = False
    is_sub: bool = False
    is_fav: bool = False
    search_request: str | None = None
    count: int = ITEMS_PER_PAGE
    offset: int = 0


class ChannelsMenuResponseData(BaseModel):
    """OUT Целевые данные для отрисовки списка каналов на 1 уровне."""

    class ChannelItem(BaseModel):
        id: int
        name: str
        is_sub: bool
        is_fav: bool

    total_count: int
    channels: list[ChannelItem] = []
