from pydantic import BaseModel


class ChannelDetailParams(BaseModel):
    """IN Получить детали по 1 каналу на 2 уровне."""
    user_id: int
    channel_id: int


class ChannelToggleSubParams(BaseModel):
    """IN Переключить подписку у канала в противоположное состояние."""
    user_id: int
    channel_id: int


class ChannelToggleFavParams(BaseModel):
    """IN Переключить избранное у канала в противоположное состояние."""
    user_id: int
    channel_id: int


class ChannelToggleReactionParams(BaseModel):
    """IN Переключить реакцию у канала в противоположное состояние."""
    user_id: int
    channel_id: int


class ChannelDetailResponseData(BaseModel):
    """OUT Целевые данные для отрисовки деталки канала на 2 уровне."""
    id: int
    name: str
    is_sub: bool
    is_fav: bool
    link: str | None = None
    members: int | None = None
    tracked_count: int
    likes: int
    dislikes: int
    user_reaction: bool | None = None
