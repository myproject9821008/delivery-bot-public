from pydantic import BaseModel


class ChatDetailParams(BaseModel):
    """IN Получить детали по 1 чату на 2 уровне"""
    user_id: int
    chat_id: int


class ChatToggleSubParams(BaseModel):
    """IN Переключить подписку у выбранного чата в противоположное состояние."""
    user_id: int
    chat_id: int


class ChatToggleFavParams(BaseModel):
    """IN Переключить избранность у чата в противоположное состояние."""
    user_id: int
    chat_id: int


class ChatToggleReactionParams(BaseModel):
    """IN Переключить реакцию у чата в противоположное состояние."""
    user_id: int
    chat_id: int


class ChatDetailResponseData(BaseModel):
    """OUT Целевые данные для отрисовки деталки по одному чату на 2 уровне."""
    id: int
    name: str
    is_sub: bool
    is_fav: bool
    link: str | None = None
    members: int | None = None
    tracked_count: int
    likes: int
    dislikes: int
    user_reaction: bool | None = None