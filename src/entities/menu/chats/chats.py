from pydantic import BaseModel
from config import ITEMS_PER_PAGE


class ChatsMenuParams(BaseModel):
    """IN Получить список чатов на 1 уровне."""
    user_id: int
    is_top: bool = False
    is_sub: bool = False
    is_fav: bool = False
    search_request: str | None = None
    count: int = ITEMS_PER_PAGE
    offset: int = 0


class ChatsMenuToggleSubParams(BaseModel):
    """IN Переключить подписку на чат в противоположное состояние."""
    user_id: int
    chat_id: int
    is_top: bool = False
    is_sub: bool = False
    is_fav: bool = False
    search_request: str | None = None
    count: int = ITEMS_PER_PAGE
    offset: int = 0


class ChatsMenuResponseData(BaseModel):
    """OUT Целевые данные для отрисовки списка чатов на 1 уровне."""

    class ChatItem(BaseModel):
        id: int
        name: str
        is_sub: bool
        is_fav: bool

    total_count: int
    chats: list[ChatItem] = []
