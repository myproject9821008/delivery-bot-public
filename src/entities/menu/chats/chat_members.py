from pydantic import BaseModel
from config import ITEMS_PER_PAGE


class ChatMembersParams(BaseModel):
    """IN Получить список участников чата на 3 уровне."""
    user_id: int
    chat_id: int
    is_sub: bool = False
    search_request: str | None = None
    count: int = ITEMS_PER_PAGE
    offset: int = 0


class ChatMembersToggleSubParams(BaseModel):
    """IN Переключить подписку на участника чата в противоположное состояние."""
    user_id: int
    chat_id: int
    member_id: int
    is_sub: bool = False
    search_request: str | None = None
    count: int = ITEMS_PER_PAGE
    offset: int = 0


class ChatMembersOffAllSubParams(BaseModel):
    """IN Выключить подписку всех участников чата."""
    user_id: int
    chat_id: int
    is_sub: bool = False
    search_request: str | None = None
    count: int = ITEMS_PER_PAGE
    offset: int = 0


class ChatMembersResponseData(BaseModel):
    """OUT Целевые данные для отрисовки списка участников чата на 3 уровне."""

    class MemberItem(BaseModel):
        id: int
        name: str
        is_sub: bool

    total_count: int
    chat_name: str
    members: list[MemberItem] = []
