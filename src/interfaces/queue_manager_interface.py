from abc import ABC, abstractmethod


class QueueManagerInterface(ABC):
    @abstractmethod
    async def run_message_queue(self) -> None:
        raise NotImplementedError

    @abstractmethod
    async def add_to_queue(self, request_id: int, priority: int) -> None:
        raise NotImplementedError
