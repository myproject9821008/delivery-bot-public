from abc import ABC, abstractmethod

from entities.menu.channels.channel_details import ChannelDetailResponseData
from entities.menu.channels.channels import ChannelsMenuResponseData
from entities.menu.chats.chat_details import ChatDetailResponseData
from entities.menu.chats.chat_members import ChatMembersResponseData
from entities.menu.chats.chats import ChatsMenuResponseData
from entities.menu.members.member_chats import MemberChatsResponseData
from entities.menu.members.member_details import MemberDetailResponseData
from entities.menu.members.members import MembersMenuResponseData
from schemas.handler_schemas import (
    # 1st lvl menu
    ChatsSchema, ChannelsSchema, MembersSchema,
    # 2nd lvl menu
    ChatConfSchema, MemberConfSchema,
    # 3rd lvl menu
    ChMembersSchema, MemberChSchema, ChanConfSchema,
)


# region MAIN CLASS THINGS
##########################
class MainClassInterface(ABC):
    @abstractmethod
    async def check_subscribe(self, user_id) -> bool:
        raise NotImplementedError


# endregion MAIN CLASS THINGS
# region 1ST LEVEL MENU
#######################
class ChatsListingMenuDataInterface(ABC):
    @abstractmethod
    async def get_chats_listing(self, frame: ChatsSchema) -> ChatsMenuResponseData:
        raise NotImplementedError

    @abstractmethod
    async def change_chats_listing(self, frame: ChatsSchema) -> ChatsMenuResponseData:
        raise NotImplementedError


class ChannelsListingMenuDataInterface(ABC):
    @abstractmethod
    async def get_channels_listing(self, frame: ChannelsSchema) -> ChannelsMenuResponseData:
        raise NotImplementedError

    @abstractmethod
    async def change_channels_listing(self, frame: ChannelsSchema) -> ChannelsMenuResponseData:
        raise NotImplementedError


class PeoplesListingMenuDataInterface(ABC):
    @abstractmethod
    async def get_members_listing(self, frame: MembersSchema) -> MembersMenuResponseData:
        raise NotImplementedError

    @abstractmethod
    async def change_members_listing(self, frame: MembersSchema) -> MembersMenuResponseData:
        raise NotImplementedError


# endregion 1ST LEVEL MENU
# region 2ND LEVEL MENU
#######################
class OneChatDetailsMenuDataInterface(ABC):
    @abstractmethod
    async def get_one_chat(self, frame: ChatConfSchema) -> ChatDetailResponseData:
        raise NotImplementedError

    @abstractmethod
    async def change_one_chat(self, frame: ChatConfSchema) -> ChatDetailResponseData:
        raise NotImplementedError


class OneChannelDetailMenuDataInterface(ABC):
    @abstractmethod
    async def get_one_channel(self, frame: ChanConfSchema) -> ChannelDetailResponseData:
        raise NotImplementedError

    @abstractmethod
    async def change_one_channel(self, frame: ChanConfSchema) -> ChannelDetailResponseData:
        raise NotImplementedError


class OnePeopleDetailMenuDataInterface(ABC):
    @abstractmethod
    async def get_one_member(self, frame: MemberConfSchema) -> MemberDetailResponseData:
        raise NotImplementedError

    @abstractmethod
    async def change_one_member(self, frame: MemberConfSchema) -> MemberDetailResponseData:
        raise NotImplementedError


# endregion 2ND LEVEL MENU
# region 3RD LEVEL MENU
#######################
class OneChatPeopleDetailsMenuDataInterface(ABC):
    @abstractmethod
    async def get_chat_members_listing(self, frame: ChMembersSchema) -> ChatMembersResponseData:
        raise NotImplementedError

    @abstractmethod
    async def change_chat_members_listing(self, frame: ChMembersSchema) -> ChatMembersResponseData:
        raise NotImplementedError

    @abstractmethod
    async def off_all_chat_members(self, frame: ChMembersSchema) -> ChatMembersResponseData:
        raise NotImplementedError


class OnePeopleChatDetailsMenuDataInterface(ABC):
    @abstractmethod
    async def get_member_chats(self, frame: MemberChSchema) -> MemberChatsResponseData:
        raise NotImplementedError

    @abstractmethod
    async def change_member_chats_listing(self, frame: MemberChSchema) -> MemberChatsResponseData:
        raise NotImplementedError

    @abstractmethod
    async def off_all_member_chats(self, frame: MemberChSchema) -> MemberChatsResponseData:
        raise NotImplementedError


# endregion 2ND LEVEL MENU
# region ALL-IN-ONE INTERFACE
#############################
class DataAdapterInterface(
    MainClassInterface,
    ChatsListingMenuDataInterface,
    ChannelsListingMenuDataInterface,
    PeoplesListingMenuDataInterface,
    OneChatDetailsMenuDataInterface,
    OneChannelDetailMenuDataInterface,
    OnePeopleDetailMenuDataInterface,
    OneChatPeopleDetailsMenuDataInterface,
    OnePeopleChatDetailsMenuDataInterface,
    ABC
):
    pass
