from abc import ABC, abstractmethod
from typing import Any

from telethon import TelegramClient
from telethon.events import CallbackQuery

from interfaces.cache_manager_interface import CacheManagerInterface
from interfaces.data_adapter_interface import DataAdapterInterface
from interfaces.listener_manager_interface import ListenersManagerInterface
from interfaces.send_manager_interface import SendManagerInterface


class UIHandlerInterface(ABC):
    @property
    @abstractmethod
    def client(self) -> TelegramClient:
        raise NotImplementedError

    @property
    @abstractmethod
    def listener(self) -> ListenersManagerInterface:
        raise NotImplementedError

    @property
    @abstractmethod
    def cache_manager(self) -> CacheManagerInterface:
        raise NotImplementedError

    @property
    @abstractmethod
    def send_manager(self) -> SendManagerInterface:
        raise NotImplementedError

    @property
    @abstractmethod
    def data(self) -> DataAdapterInterface:
        raise NotImplementedError

    @property
    @abstractmethod
    def handlers(self) -> list:
        raise NotImplementedError

    @abstractmethod
    async def bind(self) -> None:
        raise NotImplementedError


class UIInlineHandlerInterface(UIHandlerInterface, ABC):
    @staticmethod
    @abstractmethod
    async def get_inline_params(event: CallbackQuery.Event) -> Any:
        raise NotImplementedError


class UIKeyboardHandlerInterface(UIHandlerInterface, ABC):
    pass
