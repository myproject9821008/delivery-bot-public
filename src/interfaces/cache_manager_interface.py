from abc import ABC, abstractmethod
from pathlib import Path

from telethon import TelegramClient
from telethon.tl.types import InputPhoto, InputDocument
from telethon.tl.custom import Message


class CacheManagerInterface(ABC):
    @property
    @abstractmethod
    def client(self) -> TelegramClient:
        raise NotImplementedError

    @abstractmethod
    async def preload(self) -> None:
        raise NotImplementedError

    @abstractmethod
    def get_file(self, filename: str | None = None) -> Path | InputPhoto | InputDocument:
        raise NotImplementedError

    @abstractmethod
    def add_file(self, message: Message, filepath: Path) -> None:
        raise NotImplementedError
