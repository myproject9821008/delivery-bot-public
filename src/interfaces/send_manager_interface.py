from abc import ABC, abstractmethod
from typing import Callable


class SendManagerInterface(ABC):
    @abstractmethod
    async def exec(self, func: Callable, *args, **kwargs) -> None:
        raise NotImplementedError
