from abc import ABC, abstractmethod

from telethon import TelegramClient

from interfaces.send_manager_interface import SendManagerInterface


class ListenersManagerInterface(ABC):
    @property
    @abstractmethod
    def client(self) -> TelegramClient:
        raise NotImplementedError

    @property
    @abstractmethod
    def send_manager(self) -> SendManagerInterface:
        raise NotImplementedError

    @abstractmethod
    async def add_task(self, uc_class, event, handler_name: str, message_id: int, call_me_data: str) -> None:
        raise NotImplementedError

    @abstractmethod
    async def delete_task(self, chat_id) -> None:
        raise NotImplementedError
