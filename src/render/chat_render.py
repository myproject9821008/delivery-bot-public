from pathlib import Path

from telethon import custom
from telethon.tl.types import InputPhoto, InputDocument, KeyboardButtonCallback

from entities.menu.chats import ChatsMenuResponseData, ChatDetailResponseData, ChatMembersResponseData
from interfaces.cache_manager_interface import CacheManagerInterface
from schemas.command_schemas import Handler, Action
from schemas.handler_schemas import ChatsSchema, ChatConfSchema, ChMembersSchema
from src.render.base_render import BaseRender, OPTIONS_MARK, CHECKBOX_MARK, FAVORITE_MARK, RESET_FILTERS_MARK, \
    SEARCH_MARK


class ChatRender(BaseRender):
    def __init__(self, frame: ChatsSchema, data: ChatsMenuResponseData, cache_manager: CacheManagerInterface):
        self.frame = frame
        self.data = data
        self.cache_manager = cache_manager

    async def text(self) -> str:
        message_text = 'Выберите чаты, которые попадут в вашу ленту \n' \
                       '/run - старт ленты \n' \
                       '/pause - остановка ленты \n'

        search_text = f'Результаты запроса "{self.frame.custom_filter}":'
        if self.frame.custom_filter:
            return search_text
        return message_text

    async def buttons(self) -> list[list[KeyboardButtonCallback]] | None:
        line_menu_btn = await self.get_line_cursor_menu(frame=self.frame)

        top_btn = await self.get_top_btn(frame=self.frame, text='Топ чатов')

        data_buttons = await self.get_buttons_list_with_options(
            frame=self.frame,
            buttons_data=self.data['selection'],
            child_frame=Handler.CHAT_CONF,
        )

        pagination_btn = await self.get_pagination_buttons(frame=self.frame, total_count=self.data['total_count'], )

        search_btn = [custom.Button.inline(text=f'{SEARCH_MARK} Поиск', data=f'{self.frame.name}##{Action.SEARCH}##')]

        line_filters_menu = await self.get_three_btn_filter(frame=self.frame)

        drop_filters_text = f'{RESET_FILTERS_MARK} Сбросить фильтры'
        drop_filters_btn = [custom.Button.inline(text=drop_filters_text, data=f'{self.frame.name}####')]

        back_btn = [custom.Button.inline(text='↩ Обратно в меню', data=Handler.MAIN_MENU)]

        keyboard = [
            line_menu_btn,
            top_btn,
            *data_buttons,
            pagination_btn,
            search_btn,
            line_filters_menu,
            drop_filters_btn,
            back_btn
        ]
        return keyboard

    async def file(self) -> Path | InputPhoto | InputDocument | None:
        default_file = self.cache_manager.get_file('left.mp4')
        selected_file = await self.select_tape_image(
            frame=self.frame,
            current_file=default_file,
            cache_manager=self.cache_manager
        )
        return selected_file


class ChatConfRender(BaseRender):
    def __init__(self, frame: ChatConfSchema, data: ChatDetailResponseData, cache_manager: CacheManagerInterface):
        self.frame = frame
        self.data = data
        self.cache_manager = cache_manager

    async def text(self) -> str:
        chat_name = self.data['name']
        message_text = f'Настройки чата {chat_name} \n- Участников: 243 \n- Уже отслеживают: 25'
        return message_text

    async def buttons(self) -> list[list[KeyboardButtonCallback]] | None:
        chat_sub = self.data['sub']
        chat_favorite = self.data['favorite']

        sub_mark = CHECKBOX_MARK[int(chat_sub)]
        sub_text = f'{sub_mark} Отслеживается {sub_mark}' if chat_sub else f'{sub_mark} Отслеживать {sub_mark}'
        sub_data = f'{self.frame.name}##{Action.SUBSCRIPTION}##{self.frame.payload}'
        sub_btn = [custom.Button.inline(text=sub_text, data=sub_data)]

        fav_mark = FAVORITE_MARK[int(chat_favorite)]
        fav_text = f'{fav_mark} В избранном {fav_mark}' if chat_favorite else f'{fav_mark} Добавить в избранное {fav_mark}'
        fav_data = f'{self.frame.name}##{Action.FAVORITE}##{self.frame.payload}'
        fav_btn = [custom.Button.inline(text=fav_text, data=fav_data)]

        members_text = f'Участники чата {OPTIONS_MARK}'
        members_btn = [custom.Button.inline(text=members_text, data=f'{Handler.CH_MEMBERS}####{self.frame.payload}')]

        parent = self.frame.parent
        back_data = f'{parent.name}#{parent.offset}##{parent.custom_filter}#{parent.common_filter}'
        back_btn = [custom.Button.inline(text='↩ Назад к чатам', data=back_data)]

        keyboard = [sub_btn, fav_btn, members_btn, back_btn]
        return keyboard

    async def file(self) -> Path | InputPhoto | InputDocument | None:
        selected_file = self.cache_manager.get_file('config.mp4')
        return selected_file


class ChMembersRender(BaseRender):
    def __init__(self, frame: ChMembersSchema, data: ChatMembersResponseData, cache_manager: CacheManagerInterface):
        self.frame = frame
        self.data = data
        self.cache_manager = cache_manager

    async def text(self) -> str:
        chat_name = self.data['chat_name']
        search_text = f'Результаты запроса "{self.frame.custom_filter}":'
        message_text = f'Участники чата {chat_name} \n' \
                       'Мы отобрали самых заметных лидеров мнений \n' \
                       'Кого вы хотите видеть в вашей ленте?'

        message_text = message_text if not self.frame.custom_filter else search_text
        return message_text

    async def buttons(self) -> list[list[KeyboardButtonCallback]] | None:
        checkbox_buttons = await self.get_buttons_list(frame=self.frame, buttons_data=self.data['selection'])

        pagination_buttons = await self.get_pagination_buttons(frame=self.frame, total_count=self.data['total_count'])

        search_text = f'{SEARCH_MARK} Поиск'
        search_data = f'{self.frame.name}##{Action.SEARCH}##{self.frame.payload}'
        search_btn = [custom.Button.inline(text=search_text, data=search_data)]

        switch_all_btn = await self.get_reset_all_btn(self.frame)

        line_filters_menu = await self.get_two_btn_filter(frame=self.frame)

        drop_filters_text = f'{RESET_FILTERS_MARK} Сбросить фильтры'
        drop_filters_data = f'{self.frame.name}####{self.frame.payload}'
        drop_filters_btn = [custom.Button.inline(text=drop_filters_text, data=drop_filters_data)]

        parent = self.frame.parent
        to_chats_data = f'{parent.name}#{parent.offset}##{parent.custom_filter}#{parent.common_filter}'
        to_chats_btn = [custom.Button.inline(text='↩ Назад к чатам', data=to_chats_data)]

        back_text = '↩ Обратно в настройки'
        back_btn = [custom.Button.inline(text=back_text, data=f'{Handler.CHAT_CONF}####{self.frame.payload}')]

        keyboard = [
            *checkbox_buttons,
            pagination_buttons,
            search_btn,
            switch_all_btn,
            line_filters_menu,
            drop_filters_btn,
            to_chats_btn,
            back_btn
        ]
        return keyboard

    async def file(self) -> Path | InputPhoto | InputDocument | None:
        default_file = self.cache_manager.get_file('dynamic_head.mp4')
        selected_file = await self.select_tape_image(
            frame=self.frame,
            current_file=default_file,
            cache_manager=self.cache_manager
        )
        return selected_file
