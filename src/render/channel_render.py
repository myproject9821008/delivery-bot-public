from pathlib import Path

from telethon import custom
from telethon.tl.types import InputPhoto, InputDocument, KeyboardButtonCallback

from entities.menu.channels import ChannelDetailResponseData, ChannelsMenuResponseData

from interfaces.cache_manager_interface import CacheManagerInterface
from schemas.command_schemas import Handler, Action
from schemas.handler_schemas import ChanConfSchema, ChannelsSchema
from src.render.base_render import BaseRender, CHECKBOX_MARK, FAVORITE_MARK, RESET_FILTERS_MARK, SEARCH_MARK


class ChannelRender(BaseRender):
    def __init__(self, frame: ChannelsSchema, data: ChannelsMenuResponseData, cache_manager: CacheManagerInterface):
        self.frame = frame
        self.data = data
        self.cache_manager = cache_manager

    async def text(self) -> str:
        message_text = 'Выберите каналы, которые попадут в вашу ленту \n' \
                       '/run - старт ленты \n' \
                       '/pause - остановка ленты \n'

        search_text = f'Результаты запроса "{self.frame.custom_filter}":'
        if self.frame.custom_filter:
            return search_text
        return message_text

    async def buttons(self) -> list[list[KeyboardButtonCallback]] | None:
        line_menu = await self.get_line_cursor_menu(self.frame)

        top_btn = await self.get_top_btn(frame=self.frame, text='Топ каналов')

        checkbox_buttons = await self.get_buttons_list_with_options(
            frame=self.frame,
            buttons_data=self.data['selection'],
            child_frame=Handler.CHAN_CONF
        )
        pagination_buttons = await self.get_pagination_buttons(frame=self.frame, total_count=self.data['total_count'])

        search_text = f'{SEARCH_MARK} Поиск'
        search_btn = [custom.Button.inline(text=search_text, data=f'{self.frame.name}##{Action.SEARCH}##')]

        line_filters_menu = await self.get_three_btn_filter(frame=self.frame)

        reset_filters_text = f'{RESET_FILTERS_MARK} Сбросить фильтры'
        reset_filters_btn = [custom.Button.inline(text=reset_filters_text, data=f'{self.frame.name}####')]
        back_btn = [custom.Button.inline(text='↩ Обратно в меню', data='main_menu')]

        keyboard = [
            line_menu,
            top_btn,
            *checkbox_buttons,
            pagination_buttons,
            search_btn,
            line_filters_menu,
            reset_filters_btn,
            back_btn
        ]
        return keyboard

    async def file(self) -> Path | InputPhoto | InputDocument | None:
        default_file = self.cache_manager.get_file('bottom.mp4')
        selected_file = await self.select_tape_image(
            frame=self.frame,
            current_file=default_file,
            cache_manager=self.cache_manager
        )
        return selected_file


class ChanConfRender(BaseRender):
    def __init__(self, frame: ChanConfSchema, data: ChannelDetailResponseData, cache_manager: CacheManagerInterface):
        self.frame = frame
        self.data = data
        self.cache_manager = cache_manager

    async def text(self) -> str:
        channel_name = self.data['name']
        message_text = f'Настройки канала {channel_name} \n- Участников: 243 \n- Уже отслеживают: 25 \n- <Ссылка на канал>'
        return message_text

    async def buttons(self) -> list[list[KeyboardButtonCallback]] | None:
        channel_sub = self.data['sub']
        channel_favorite = self.data['favorite']

        sub_mark = CHECKBOX_MARK[int(channel_sub)]
        sub_text = f'{sub_mark} Отслеживается {sub_mark}' if channel_sub else f'{sub_mark} Отслеживать {sub_mark}'
        sub_data = f'{self.frame.name}##{Action.SUBSCRIPTION}##{self.frame.payload}'
        sub_btn = [custom.Button.inline(text=sub_text, data=sub_data)]

        fav_mark = FAVORITE_MARK[int(channel_favorite)]
        fav_data = f'{self.frame.name}##{Action.FAVORITE}##{self.frame.payload}'
        fav_text = f'{fav_mark} В избранном {fav_mark}' if channel_favorite else f'{fav_mark} Добавить в избранное {fav_mark}'
        fav_btn = [custom.Button.inline(text=fav_text, data=fav_data)]

        parent = self.frame.parent
        back_data = f'{parent.name}#{parent.offset}##{parent.custom_filter}#{parent.common_filter}'
        back_btn = [custom.Button.inline(text='↩ Назад к каналам', data=back_data)]

        keyboard = [sub_btn, fav_btn, back_btn]
        return keyboard

    async def file(self) -> Path | InputPhoto | InputDocument | None:
        selected_file = self.cache_manager.get_file('config.mp4')
        return selected_file
