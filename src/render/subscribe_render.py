from pathlib import Path

from telethon import custom
from telethon.tl.types import InputDocument, KeyboardButtonCallback, InputPhoto

from interfaces.cache_manager_interface import CacheManagerInterface
from render.base_render import BaseRender
from schemas.command_schemas import Handler
from src.config import SUBSCRIBE_LINK


class SubscribeRender(BaseRender):
    def __init__(self, cache_manager: CacheManagerInterface):
        self.cache_manager = cache_manager

    async def text(self) -> str:
        message_text = f'Подпишитесь на наш канал, чтобы продолжить работу'
        return message_text

    async def buttons(self) -> list[list[KeyboardButtonCallback]] | None:
        keyboard = [
            [custom.Button.url('Подписаться', url=SUBSCRIBE_LINK)],
            [custom.Button.inline('Продолжить', data=Handler.SUBSCRIPTION)],
        ]
        return keyboard

    async def file(self) -> Path | InputPhoto | InputDocument | None:
        default_file = self.cache_manager.get_file('dynamic_head.mp4')
        return default_file
