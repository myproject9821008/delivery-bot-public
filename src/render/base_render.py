from abc import ABC, abstractmethod
from pathlib import Path
from typing import NewType

from pydantic import BaseModel, ConfigDict
from telethon import custom
from telethon.tl.types import InputPhoto, InputDocument, KeyboardButtonCallback, KeyboardButtonUrl, \
    KeyboardButtonWebView

from schemas.command_schemas import CommonFilter, CustomFilter, Handler, Action
from schemas.handler_schemas import FrameCommonType
from src.config import PAG_SKIP_COUNT, PAG_SKIP_BUTTONS


CHECKBOX_MARK = ('❌', '✅')
FAVORITE_MARK = ('', '⭐')
NEXT_MARK = ('◀', '▶')
END_MARK = ('⏮', '⏭')
EMPTY_MARK = ' '
OPTIONS_MARK = '➡'
CWITCH_ALL_MARK = '❌ Выкл все ❌'
EMPTY_LIST_MARK = '⚠⚠⚠ Нет результатов ⚠⚠⚠'
NO_PAGES_MARK = '...'
RESET_FILTERS_MARK = '🔄'
SEARCH_MARK = '🔍'


LINE_MENU_DATA = {
            'Чаты': Handler.CHATS,
            'Каналы': Handler.CHANNELS,
            'Люди': Handler.MEMBERS,
        }


CATALOG_FILTERS = {
    'Все': CommonFilter.EMPTY,
    'Подписки': CommonFilter.SUBSCRIPTION,
    'Избранное': CommonFilter.FAVORITE
}


ITEM_FILTERS = {
    'Все': CommonFilter.EMPTY,
    'Подписки': CommonFilter.SUBSCRIPTION
}

ButtonsType = NewType('ButtonsType', list[list[KeyboardButtonCallback | KeyboardButtonUrl | KeyboardButtonWebView]])


class BaseRender(ABC):
    class RenderCommonOutputDTO(BaseModel):
        model_config = ConfigDict(arbitrary_types_allowed=True)

        text: str
        file: Path | InputPhoto | InputDocument | None = None
        buttons: ButtonsType | None = None

    async def execute(self) -> RenderCommonOutputDTO:
        return self.RenderCommonOutputDTO(
            text=await self.text(),
            buttons=await self.buttons(),
            file=await self.file(),
        )

    @abstractmethod
    async def text(self) -> str:
        raise NotImplementedError

    @abstractmethod
    async def buttons(self) -> ButtonsType | None:
        raise NotImplementedError

    @abstractmethod
    async def file(self) -> Path | InputPhoto | InputDocument | None:
        raise NotImplementedError

    @staticmethod
    async def get_reset_all_btn(frame: FrameCommonType) -> list:
        btn_data = f'{frame.name}#{frame.offset}#{Action.SWITCH_ALL}#{frame.custom_filter}#{frame.common_filter}{frame.payload}'
        button = [custom.Button.inline(text=CWITCH_ALL_MARK, data=btn_data)]
        return button

    @staticmethod
    async def get_line_cursor_menu(frame: FrameCommonType) -> list:
        buttons = []
        for key, val in LINE_MENU_DATA.items():
            btn_text = f'🔸 {key}' if val == frame.name else key
            btn = custom.Button.inline(text=btn_text, data=f'{val}###{frame.custom_filter}#')
            buttons.append(btn)
        return buttons

    @staticmethod
    async def get_three_btn_filter(frame: FrameCommonType) -> list:
        buttons = []
        for btn_name, common_filter in CATALOG_FILTERS.items():
            btn_text = f'🔸 {btn_name}' if common_filter == frame.common_filter else btn_name
            btn = custom.Button.inline(text=btn_text, data=f'{frame.name}###{frame.custom_filter}#{common_filter}')
            buttons.append(btn)
        return buttons

    @staticmethod
    async def get_two_btn_filter(frame: FrameCommonType) -> list:
        buttons = []
        for btn_name, common_filter in ITEM_FILTERS.items():
            btn_text = f'🔸 {btn_name}' if common_filter == frame.common_filter else btn_name
            btn = custom.Button.inline(text=btn_text, data=f'{frame.name}###{frame.custom_filter}#{common_filter}{frame.payload}')
            buttons.append(btn)
        return buttons

    @staticmethod
    async def get_top_btn(frame: FrameCommonType, text: str) -> list:
        top_mark = f'🔸 💥{text}💥' if frame.custom_filter == CustomFilter.TOP else f'💥{text}💥'
        btn = [custom.Button.inline(text=top_mark, data=f'{frame.name}###{CustomFilter.TOP}#')]
        return btn

    @staticmethod
    async def get_buttons_list_with_options(frame: FrameCommonType, buttons_data: dict, child_frame: str = '') -> list:
        buttons = []
        if not buttons_data:
            return [[custom.Button.inline(EMPTY_LIST_MARK)]]
        for ident, value in buttons_data.items():
            item_name = value['name']
            item_sub = value['sub']
            item_favorite = value['favorite']

            checkbox_text = f'{CHECKBOX_MARK[int(item_sub)]}{FAVORITE_MARK[int(item_favorite)]} {item_name}'
            checkbox_data = f'{frame.name}#{frame.offset}#{ident}#{frame.custom_filter}#{frame.common_filter}'
            checkbox_btn = custom.Button.inline(text=checkbox_text, data=checkbox_data)

            child_data = f'{child_frame}####/{frame.name}#{frame.offset}#{ident}#{frame.custom_filter}#{frame.common_filter}'
            child_btn = custom.Button.inline(text=f'Подробнее {OPTIONS_MARK}', data=child_data)

            buttons.append([checkbox_btn, child_btn])
        return buttons

    @staticmethod
    async def get_buttons_list(frame: FrameCommonType, buttons_data: dict) -> list:
        buttons = []
        if not buttons_data:
            return [[custom.Button.inline(EMPTY_LIST_MARK)]]
        for ident, value in buttons_data.items():
            item_name = value['name']
            item_sub = value['sub']

            checkbox_text = f'{CHECKBOX_MARK[int(item_sub)]} {item_name}'
            checkbox_data = f'{frame.name}#{frame.offset}#{ident}#{frame.custom_filter}#{frame.common_filter}{frame.payload}'
            checkbox_btn = custom.Button.inline(text=checkbox_text, data=checkbox_data)
            buttons.append([checkbox_btn])
        return buttons

    @staticmethod
    async def select_tape_image(frame: FrameCommonType, current_file, cache_manager) -> Path | InputPhoto | InputDocument:
        if frame.custom_filter == CustomFilter.TOP:
            return cache_manager.get_file('top.mp4')
        if frame.custom_filter:
            return cache_manager.get_file('search.mp4')
        return current_file

    @staticmethod
    async def get_pagination_buttons(frame: FrameCommonType, total_count: int) -> list:
        buttons = []
        count = frame.count
        offset = frame.offset
        pages_count = total_count // count if total_count % count == 0 else total_count // count + 1
        skip_buttons = PAG_SKIP_COUNT * 2 < pages_count and PAG_SKIP_BUTTONS
        empty_btn = custom.Button.inline(EMPTY_MARK, data=Handler.EMPTY)
        offset_param = '{}'
        data_string = f'{frame.name}#{offset_param}##{frame.custom_filter}#{frame.common_filter}{frame.payload}'

        backward_offset = offset - count if offset > 0 else 0

        first_page_offset = 0

        current_page_number = int(offset / count) + 1
        current_page_mark = f'{current_page_number}/{pages_count}' if pages_count > 0 else NO_PAGES_MARK

        skip_some_backward_mark = f'-{PAG_SKIP_COUNT}'
        skip_some_backward_offset = offset - count * PAG_SKIP_COUNT if offset - count * PAG_SKIP_COUNT > 0 else 0

        last_page_offset = (pages_count - 1) * count if pages_count > 0 else 0

        skip_some_forward_mark = f'+{PAG_SKIP_COUNT}'
        skip_some_forward_offset = offset + count * PAG_SKIP_COUNT if offset + count * PAG_SKIP_COUNT < total_count else last_page_offset

        forward_offset = offset + count if offset + count < total_count else offset

        # кнопка в начало
        if first_page_offset != offset:
            first_data = data_string.format(first_page_offset)
            buttons.append(custom.Button.inline(END_MARK[0], data=first_data))
        else:
            buttons.append(empty_btn)

        # кнопка назад
        if backward_offset != offset:
            back_data = data_string.format(backward_offset)
            buttons.append(custom.Button.inline(NEXT_MARK[0], data=back_data))
        else:
            buttons.append(empty_btn)

        # кнопка мотать на N назад
        if skip_buttons:
            if skip_some_backward_offset != offset:
                skip_back_data = data_string.format(skip_some_backward_offset)
                buttons.append(custom.Button.inline(skip_some_backward_mark, data=skip_back_data))
            else:
                buttons.append(empty_btn)

        # кнопка пустышка вместо мотать на N назад
        # if not skip_buttons:
        #     buttons.append(custom.Button.inline(empty_btn)

        # кнопка текущее положение
        buttons.append(custom.Button.inline(current_page_mark, data=Handler.EMPTY))

        # кнопка пустышка вместо мотать на N вперед
        # if not skip_buttons:
        #     buttons.append(custom.Button.inline(empty_btn)

        # кнопка мотать на N вперед
        if skip_buttons:
            if skip_some_forward_offset != offset:
                skip_forward_data = data_string.format(skip_some_forward_offset)
                buttons.append(custom.Button.inline(skip_some_forward_mark, data=skip_forward_data))
            else:
                buttons.append(empty_btn)

        # кнопка вперед
        if forward_offset != offset:
            forward_data = data_string.format(forward_offset)
            buttons.append(custom.Button.inline(NEXT_MARK[1], data=forward_data))
        else:
            buttons.append(empty_btn)

        # кнопка в конец
        if last_page_offset != offset:
            last_data = data_string.format(last_page_offset)
            buttons.append(custom.Button.inline(END_MARK[1], data=last_data))
        else:
            buttons.append(empty_btn)

        return buttons
