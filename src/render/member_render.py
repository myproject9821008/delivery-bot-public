from pathlib import Path

from telethon import custom
from telethon.tl.types import InputPhoto, InputDocument, KeyboardButtonCallback

from entities.menu.members import MembersMenuResponseData, MemberChatsResponseData
from entities.menu.members.member_details import MemberDetailResponseData
from interfaces.cache_manager_interface import CacheManagerInterface
from schemas.command_schemas import Handler, Action
from schemas.handler_schemas import MembersSchema, MemberConfSchema, MemberChSchema
from src.render.base_render import BaseRender, OPTIONS_MARK, CHECKBOX_MARK, FAVORITE_MARK, RESET_FILTERS_MARK, \
    SEARCH_MARK


class MemberRender(BaseRender):
    def __init__(self, frame: MembersSchema, data: MembersMenuResponseData, cache_manager: CacheManagerInterface):
        self.frame = frame
        self.data = data
        self.cache_manager = cache_manager

    async def text(self) -> str:
        message_text = 'Выберите инфлюенсеров, которых хотите отслеживать \n' \
                       'Мы отобрали самых заметных лидеров мнений \n' \
                       '/run - старт ленты \n' \
                       '/pause - остановка ленты \n'

        search_text = f'Результаты запроса "{self.frame.custom_filter}":'
        if self.frame.custom_filter:
            return search_text
        return message_text

    async def buttons(self) -> list[list[KeyboardButtonCallback]] | None:
        line_menu = await self.get_line_cursor_menu(frame=self.frame)

        top_btn = await self.get_top_btn(frame=self.frame, text='Топ инфлюенсеров')

        checkbox_buttons = await self.get_buttons_list_with_options(
            frame=self.frame,
            buttons_data=self.data['selection'],
            child_frame=Handler.MEMBER_CONF,
        )
        pagination_buttons = await self.get_pagination_buttons(frame=self.frame, total_count=self.data['total_count'])

        search_text = f'{SEARCH_MARK} Поиск'
        search_btn = [custom.Button.inline(text=search_text, data=f'{self.frame.name}##{Action.SEARCH}##')]

        line_filters_btn = await self.get_three_btn_filter(frame=self.frame)

        drop_filters_text = f'{RESET_FILTERS_MARK} Сбросить фильтры'
        drop_filters_btn = [custom.Button.inline(text=drop_filters_text, data=f'{self.frame.name}####')]

        back_btn = [custom.Button.inline(text='↩ Обратно в меню', data=Handler.MAIN_MENU)]

        keyboard = [
            line_menu,
            top_btn,
            *checkbox_buttons,
            pagination_buttons,
            search_btn,
            line_filters_btn,
            drop_filters_btn,
            back_btn
        ]
        return keyboard

    async def file(self) -> Path | InputPhoto | InputDocument | None:
        default_file = self.cache_manager.get_file('right.mp4')
        selected_file = await self.select_tape_image(
            frame=self.frame,
            current_file=default_file,
            cache_manager=self.cache_manager
        )
        return selected_file


class MemberConfRender(BaseRender):
    def __init__(self, frame: MemberConfSchema, data: MemberDetailResponseData, cache_manager: CacheManagerInterface):
        self.frame = frame
        self.data = data
        self.cache_manager = cache_manager

    async def text(self) -> str:
        member_name = self.data['name']
        message_text = f'Настройки инфлюенсера {member_name} \n- Уже отслеживают: 30 \n- Чатов в подписках: 9'
        return message_text

    async def buttons(self) -> list[list[KeyboardButtonCallback]] | None:
        member_sub = self.data['sub']
        member_favorite = self.data['favorite']

        sub_mark = CHECKBOX_MARK[int(member_sub)]
        sub_text = f'{sub_mark} Отслеживается {sub_mark}' if member_sub else f'{sub_mark} Отслеживать {sub_mark}'
        sub_data = f'{self.frame.name}##{Action.SUBSCRIPTION}##{self.frame.payload}'
        sub_btn = [custom.Button.inline(text=sub_text, data=sub_data)]

        fav_mark = FAVORITE_MARK[int(member_favorite)]
        fav_text = f'{fav_mark} В избранном {fav_mark}' if member_favorite else f'{fav_mark} Добавить в избранное {fav_mark}'
        fav_data = f'{self.frame.name}##{Action.FAVORITE}##{self.frame.payload}'
        fav_btn = [custom.Button.inline(text=fav_text, data=fav_data)]

        members_btn_text = f'Чаты пользователя {OPTIONS_MARK}'
        members_btn = [custom.Button.inline(text=members_btn_text, data=f'{Handler.MEMBER_CH}####{self.frame.payload}')]

        parent = self.frame.parent
        back_data = f'{parent.name}#{parent.offset}##{parent.custom_filter}#{parent.common_filter}'
        back_btn = [custom.Button.inline(text='↩ Назад к пользователям', data=back_data)]

        keyboard = [sub_btn, fav_btn, members_btn, back_btn]
        return keyboard

    async def file(self) -> Path | InputPhoto | InputDocument | None:
        selected_file = self.cache_manager.get_file('config.mp4')
        return selected_file


class MemberChRender(BaseRender):
    def __init__(self, frame: MemberChSchema, data: MemberChatsResponseData, cache_manager: CacheManagerInterface):
        self.frame = frame
        self.data = data
        self.cache_manager = cache_manager

    async def text(self) -> str:
        member_name = self.data['member_name']
        search_text = f'Результаты запроса "{self.frame.custom_filter}":'
        message_text = f'Чаты пользователя: {member_name} \n' \
                       'Выберите чаты в которых хотите остлеживать пользователя'
        message_text = message_text if not self.frame.custom_filter else search_text
        return message_text

    async def buttons(self) -> list[list[KeyboardButtonCallback]] | None:
        checkbox_buttons = await self.get_buttons_list(frame=self.frame, buttons_data=self.data['selection'])

        pagination_buttons = await self.get_pagination_buttons(frame=self.frame, total_count=self.data['total_count'])

        search_text = f'{SEARCH_MARK} Поиск'
        search_data = f'{self.frame.name}##{Action.SEARCH}##{self.frame.payload}'
        search_btn = [custom.Button.inline(text=search_text, data=search_data)]

        switch_all_btn = await self.get_reset_all_btn(frame=self.frame)

        line_filters_menu = await self.get_two_btn_filter(frame=self.frame)

        drop_filters_text = f'{RESET_FILTERS_MARK} Сбросить фильтры'
        drop_filters_data = f'{self.frame.name}####{self.frame.payload}'
        drop_filters_btn = [custom.Button.inline(text=drop_filters_text, data=drop_filters_data)]

        parent = self.frame.parent
        to_chats_data = f'{parent.name}#{parent.offset}##{parent.custom_filter}#{parent.common_filter}'
        to_chats_btn = [custom.Button.inline(text='↩ Назад к пользователям', data=to_chats_data)]

        back_btn_text = '↩ Обратно в настройки'
        back_btn = [custom.Button.inline(text=back_btn_text, data=f'{Handler.MEMBER_CONF}####{self.frame.payload}')]

        keyboard = [
            *checkbox_buttons,
            pagination_buttons,
            search_btn,
            switch_all_btn,
            line_filters_menu,
            drop_filters_btn,
            to_chats_btn,
            back_btn
        ]
        return keyboard

    async def file(self) -> Path | InputPhoto | InputDocument | None:
        default_file = self.cache_manager.get_file('dynamic_head.mp4')
        selected_file = await self.select_tape_image(
            frame=self.frame,
            current_file=default_file,
            cache_manager=self.cache_manager
        )
        return selected_file
