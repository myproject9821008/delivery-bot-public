from pathlib import Path

from telethon import custom
from telethon.tl.types import KeyboardButtonWebView, KeyboardButtonCallback, InputPhoto, InputDocument

from interfaces.cache_manager_interface import CacheManagerInterface
from render.base_render import BaseRender
from schemas.command_schemas import Handler


class MainMenuRender(BaseRender):
    def __init__(self, cache_manager: CacheManagerInterface):
        self.cache_manager = cache_manager

    async def text(self) -> str:
        # message_text = 'Hello this is 🧑‍🚒 [Главное меню](spoiler)'
        message_text = 'Настройте свою ленту, чтобы отслеживать самую ценную информацию без рекламы и лишнего шума. \n' \
                       '/run - старт ленты \n' \
                       '/pause - остановка ленты'
        return message_text

    async def buttons(self) -> list[list[KeyboardButtonCallback]] | None:
        keyboard = [
            [custom.Button.inline('📰 Моя лента', data=f'{Handler.CHATS}####')],
            [custom.Button.url('🧑‍🚒 Поддержка', url='https://www.google.com/')],
            [KeyboardButtonWebView(text='Тестовая кнопка', url='https://output.jsbin.com/rubuticaqi.html')]
        ]
        return keyboard

    async def file(self) -> Path | InputPhoto | InputDocument | None:
        default_file = self.cache_manager.get_file('dynamic_head.mp4')
        return default_file
