import asyncio

from telethon import TelegramClient

from adapters.db_adapter import DataAdapter
from config import BOT_TOKEN, API_HASH, API_ID
from handlers.base_handlers import HandlerParams
from handlers.handler_utils import QueueManager
from handlers.handler_utils import SendManager, ListenersManager
from shared.cache_manager import CacheManager
from shared.common_utils import CustomMarkdown
from src.handlers.inline_handler import InlineHandler
from src.handlers.keyboard_handler import KeyboardHandler


async def main() -> None:
    client = TelegramClient(session='session_name', api_id=API_ID, api_hash=API_HASH)
    client.parse_mode = CustomMarkdown()

    cache_manager = CacheManager(client=client)
    queue_manager = QueueManager()
    loop = asyncio.get_event_loop()
    loop.create_task(queue_manager.run_message_queue())

    send_manager = SendManager(cache_manager=cache_manager, queue_manager=queue_manager)

    params = HandlerParams(
        client=client,
        listener=ListenersManager(client=client, send_manager=send_manager),
        data_adapter=DataAdapter(data_source='stub_value'),
        cache_manager=cache_manager,
        send_manager=send_manager,
    )
    await InlineHandler(params=params).bind()
    await KeyboardHandler(params=params).bind()
    await params.cache_manager.preload()

    await client.start(bot_token=BOT_TOKEN)
    print('Application started!')
    await client.run_until_disconnected()


if __name__ == '__main__':
    asyncio.run(main())
