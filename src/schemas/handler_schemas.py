from typing import TypeVar

from pydantic import BaseModel

from schemas.command_schemas import Handler, CommonFilter

FrameCommonType = TypeVar(
    'FrameCommonType',
    'ChatsSchema',  'ChannelsSchema', 'MembersSchema',
    'ChatConfSchema', 'ChanConfSchema', 'MemberConfSchema',
    'ChMembersSchema', 'MemberChSchema',
)


class BaseSchema(BaseModel):
    name: Handler
    action: str
    sender: int | None = None
    chat: int | None = None
    message_id: int | None = None
    parent: FrameCommonType | None = None
    payload: str = ''


class ChannelsSchema(BaseSchema):
    """Меню каналов 1 уровень"""
    offset: int
    count: int
    custom_filter: str
    common_filter: CommonFilter


class ChanConfSchema(BaseSchema):
    """Деталка канала 2 уровень"""
    pass


class ChatsSchema(BaseSchema):
    """Меню чатов 1 уровень"""
    offset: int
    count: int
    custom_filter: str
    common_filter: CommonFilter


class ChatConfSchema(BaseSchema):
    """Деталка чата 2 уровень"""
    pass


class ChMembersSchema(BaseSchema):
    """Список мемберов в выбранном чате (переход из деталки чата) чата 3 уровень"""
    offset: int
    count: int
    custom_filter: str
    common_filter: CommonFilter


class MembersSchema(BaseSchema):
    """Меню людей 1 уровень"""
    offset: int
    count: int
    custom_filter: str
    common_filter: CommonFilter


class MemberConfSchema(BaseSchema):
    """Деталка человека 2 уровень"""
    pass


class MemberChSchema(BaseSchema):
    """Список чатов человека 3 уровень"""
    offset: int
    count: int
    custom_filter: str
    common_filter: CommonFilter
