from typing import NewType, Dict
import re
from telethon.events import CallbackQuery

from schemas.handler_schemas import FrameCommonType, BaseSchema
from schemas import handler_schemas


FrameSchemaStorage = NewType('FrameSchemaStorage', Dict[str, FrameCommonType])


POSITIONS = ('name', 'offset', 'action', 'custom_filter', 'common_filter')


class SchemaBuilder:
    """Package the input data into circuits with appropriate names"""
    def __init__(self, params: list, payload: str, event: CallbackQuery.Event, count: int):
        self.__params = params
        self.__payload = payload
        self.__schemas: FrameSchemaStorage = FrameSchemaStorage({})
        self.__event = event
        self.__count = count

        # schemes are collected from the file handler_schemas

        for name, cls in handler_schemas.__dict__.items():
            if isinstance(cls, BaseSchema.__class__) and cls is not BaseSchema and not name.startswith('_') and name.endswith('Schema'):
                new_name = self.__get_snakecase_name(class_name=name)
                self.__schemas[new_name] = cls

    async def execute(self) -> FrameCommonType:
        self.__params.reverse()
        building_object = None

        main_element = self.__params.pop()
        main_element_schema = self.__schemas.get(main_element[0])
        if not main_element_schema:
            raise Exception(f'You forgot to add a schema for command: {main_element[0]}')

        for params_group in self.__params:
            schema = self.__schemas.get(params_group[0])
            if not schema:
                raise Exception(f'You forgot to add a schema for command: {params_group[0]}')

            building_object = await self.__fill_object(
                schema=schema,
                positional_params=params_group,
                last_object=building_object,
                count=self.__count
            )

        event_params = await self.get_params_from_event(event=self.__event)
        building_object = await self.__fill_object(
            schema=main_element_schema,
            positional_params=main_element,
            event_params=event_params,
            last_object=building_object,
            payload=self.__payload,
            count=self.__count
        )
        return building_object

    @staticmethod
    def __get_snakecase_name(class_name: str) -> str:
        """Returns the snake case name without 'Schema'"""
        uniq_name = class_name.replace('Schema', '')
        pattern = re.compile(r'(?<!^)(?=[A-Z])')
        search_name = pattern.sub('_', uniq_name).lower()
        return search_name

    @staticmethod
    async def __fill_object(
            schema: FrameCommonType,
            positional_params: list,
            count: int,
            event_params: dict | None = None,
            payload: str = '',
            last_object: FrameCommonType | None = None):

        zipped = zip(POSITIONS, positional_params)
        params_to_fill = dict(zipped)

        if event_params:
            params_to_fill.update(event_params)
        params_to_fill['payload'] = payload
        params_to_fill['offset'] = 0 if not params_to_fill['offset'] else params_to_fill['offset']
        params_to_fill['parent'] = last_object if last_object else None
        params_to_fill['count'] = count

        result = schema(**params_to_fill)
        return result

    @staticmethod
    async def get_params_from_event(event: CallbackQuery.Event) -> dict:
        result = {
            'sender': event.sender_id,
            'chat': event.chat.id,
            'message_id': event.message_id,
        }
        return result
