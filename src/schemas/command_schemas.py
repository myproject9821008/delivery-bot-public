from enum import Enum


class BaseEnum(str, Enum):
    def __str__(self):
        return str(self.value)


class Handler(BaseEnum):
    EMPTY = 'empty'
    CHANNELS = 'channels'
    CHATS = 'chats'
    MEMBERS = 'members'
    SUBSCRIPTION = 'subscription'
    MAIN_MENU = 'main_menu'
    CHAN_CONF = 'chan_conf'
    CHAT_CONF = 'chat_conf'
    CH_MEMBERS = 'ch_members'
    MEMBER_CONF = 'member_conf'
    MEMBER_CH = 'member_ch'


class Action(BaseEnum):
    SEARCH = 'search'
    SEARCH_RESULT = 'search_r'
    SUBSCRIPTION = 'sub'
    FAVORITE = 'fav'
    SWITCH_ALL = 'all'
    LIKE = 'li'
    DISLIKE = 'dli'
    EMPTY = ''


class CommonFilter(BaseEnum):
    SUBSCRIPTION = 'sub'
    FAVORITE = 'fav'
    EMPTY = ''


class CustomFilter(BaseEnum):
    TOP = 'top'
    EMPTY = ''
