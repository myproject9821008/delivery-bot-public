from asyncio import sleep, PriorityQueue, Event, Lock
from datetime import timedelta, datetime
from typing import Callable, NewType, Dict

from pydantic import BaseModel, ConfigDict
from telethon import events, TelegramClient
from telethon.errors import MessageNotModifiedError
from telethon.events import NewMessage


from config import MAX_SEARCH_LENGTH, GLOBAL_TIME_RANGE_SEC, GLOBAL_TASK_COUNT, STACK_FIRST_TIME_RANGE, \
    STACK_SECOND_TIME_RANGE, STACK_FIRST_TASK_COUNT, STACK_SECOND_TASK_COUNT
from interfaces.cache_manager_interface import CacheManagerInterface
from interfaces.listener_manager_interface import ListenersManagerInterface
from interfaces.queue_manager_interface import QueueManagerInterface
from interfaces.send_manager_interface import SendManagerInterface

from shared.common_utils import replace_borders


class SendManager(SendManagerInterface):
    """Управляет отправкой сообщений в Bot API"""
    def __init__(self, cache_manager, queue_manager):
        self.__cache_manager: CacheManagerInterface = cache_manager
        self.__queue_manager: QueueManagerInterface = queue_manager

    async def exec(self, func: Callable, *args, priority: int = 1, **kwargs) -> None:
        request_id = kwargs.get('entity')
        await self.__queue_manager.add_to_queue(request_id=request_id, priority=priority)

        try:
            sending_result = await func(*args, **kwargs)
            file = kwargs.get('file')
            if file:
                self.__cache_manager.add_file(sending_result, file)
        except MessageNotModifiedError:
            print(' >>> [delivery] Catching the "unmodified message" error')


class TaskData(BaseModel):
    model_config = ConfigDict(arbitrary_types_allowed=True)

    stamps: list = []
    local_tasks: list[int] = []
    local_events: dict[int, Event] = {}
    local_queue: PriorityQueue
    locker: Lock

    async def have_same_priority_tasks(self, priority: int) -> bool:
        return priority in self.local_tasks

    async def have_higher_priority_tasks(self, priority: int) -> bool:
        for task in sorted(self.local_tasks):
            if task < priority:
                return True
        return False

    async def have_lower_priority_tasks(self, priority: int) -> bool:
        for task in sorted(self.local_tasks, reverse=True):
            if task > priority:
                return True
        return False

    async def get_lower_priority_event(self, priority: int) -> Event | None:
        for i in sorted(self.local_events):
            if i > priority:
                return self.local_events[i]

    async def get_or_create_local_event(self, priority: int) -> Event:
        if priority not in self.local_events:
            self.local_events[priority] = Event()
        return self.local_events[priority]

    async def remove_task(self, priority: int) -> None:
        self.local_tasks.pop(self.local_tasks.index(priority))


QueueStorage = NewType('QueueStorage', Dict[str, TaskData])


class QueueManager(QueueManagerInterface):
    """Рассчитывает глобальные и локальные задержки, распределяет запросы по очередям"""
    def __init__(self):
        self.__personal_queues: dict[int, TaskData] = QueueStorage({})
        self.__global_queue: PriorityQueue = PriorityQueue(0)
        self.__global_stamps: list[datetime] = []
        self.__global_tasks: list = []

    async def run_message_queue(self) -> None:
        """Запускает итератор глобальной очереди"""
        while True:
            _, chat_id, global_event = await self.__global_queue.get()
            user_sending = self.__personal_queues.get(chat_id)
            self.__global_queue.task_done()

            await self.__delay(stamps=self.__global_stamps, range_sec=GLOBAL_TIME_RANGE_SEC, max_count=GLOBAL_TASK_COUNT)
            await self.__update_timestamps(user_sending)
            global_event.set()

    async def __register_request(self, request_id: int, priority: int) -> TaskData:
        """Регистрирует новый запрос в локальных очередях"""
        request_data = self.__personal_queues.get(request_id)
        if request_data is None:
            request_data = TaskData(local_queue=PriorityQueue(0), locker=Lock())
            self.__personal_queues[request_id] = request_data

        await request_data.local_queue.put((priority, request_id))
        request_data.local_tasks.append(priority)
        return request_data

    async def __local_timer(self, request_id: int, priority: int) -> None:
        """Прогоняет запрос по индивидуальным задержкам"""
        request_data = await self.__register_request(request_id, priority)

        local_event = await request_data.get_or_create_local_event(priority=priority)
        if await request_data.have_higher_priority_tasks(priority=priority):
            await local_event.wait()

        async with request_data.locker:
            await request_data.local_queue.get()
            await request_data.remove_task(priority)
            request_data.local_queue.task_done()

            await self.__delay(stamps=request_data.stamps, range_sec=STACK_FIRST_TIME_RANGE,
                               max_count=STACK_FIRST_TASK_COUNT)
            await self.__delay(stamps=request_data.stamps, range_sec=STACK_SECOND_TIME_RANGE,
                               max_count=STACK_SECOND_TASK_COUNT)

            global_event = Event()
            await self.__global_queue.put((priority, request_id, global_event))
            await global_event.wait()

            if await request_data.have_same_priority_tasks(priority=priority):
                return

            if await request_data.have_lower_priority_tasks(priority=priority):
                lower_local_event = await request_data.get_lower_priority_event(priority=priority)
                lower_local_event.set()

    async def __update_timestamps(self, request_data: TaskData) -> None:
        """Актуализирует очереди - удаляет старые таймштампы, добавляет текущий"""
        time_now = datetime.now()
        latest_global_time_point = time_now - timedelta(seconds=GLOBAL_TIME_RANGE_SEC)
        latest_personal_time_point = time_now - timedelta(seconds=max(STACK_FIRST_TIME_RANGE, STACK_SECOND_TIME_RANGE))

        is_clean = False
        while not is_clean:
            if len(self.__global_stamps) > 0 and self.__global_stamps[0] < latest_global_time_point:
                self.__global_stamps.pop(0)
            else:
                is_clean = True
        self.__global_stamps.append(time_now)

        is_clean = False
        while not is_clean:
            if len(request_data.stamps) > 0 and request_data.stamps[0] < latest_personal_time_point:
                request_data.stamps.pop(0)
            else:
                is_clean = True
        request_data.stamps.append(time_now)

    @staticmethod
    async def __delay(stamps: list, range_sec: int, max_count: int) -> None:
        """Ожидает, если превышен лимит запросов в единицу времени"""
        last_time_point = datetime.now() - timedelta(seconds=range_sec)
        last_sends = [i for i in stamps if i > last_time_point]
        sends_count = len(last_sends)

        if sends_count < max_count:
            return

        if sends_count == max_count:
            delay = last_sends[0] - last_time_point
            await sleep(delay.total_seconds())
            return

    async def add_to_queue(self, request_id: int, priority: int = 1) -> None:
        """Добавляет таск в очередь. Чем меньше 'priority', тем выше приоритет в очереди."""
        await self.__local_timer(request_id=request_id, priority=priority)


class ListenersManager(ListenersManagerInterface):
    """Один обработчик слушает один чат по запросу. {'chat_id: handler_name'}"""
    def __init__(self, client: TelegramClient, send_manager: SendManagerInterface):
        if not isinstance(send_manager, SendManagerInterface):
            raise ValueError(f'Expected SendManagerInterface, but got {type(send_manager)}: {send_manager}')

        self.__client = client
        self.__send_manager = send_manager
        self.tasks = {}
        self.__handler_class = None
        self.__initialized = False

    @property
    def client(self) -> TelegramClient:
        return self.__client

    @property
    def send_manager(self) -> SendManagerInterface:
        return self.__send_manager

    async def __bind(self, handler_class) -> None:
        if not self.__initialized:
            self.__handler_class = handler_class
            self.client.add_event_handler(self.__listen_chat, events.NewMessage(incoming=True))
            self.__initialized = True

    async def __listen_chat(self, event: NewMessage.Event):
        chat_id = event.chat_id
        if event.chat_id in self.tasks:
            text = await check_max_length_search(event=event, send_manager=self.send_manager)
            if not text:
                return

            frame_name = self.tasks[chat_id]['name']
            handler_name = f'{frame_name}_handler'
            handler_message_id = self.tasks[chat_id]['message_id']
            usecase = getattr(self.__handler_class, handler_name)

            safe_message = replace_borders(text)
            command = self.tasks[chat_id]['call_me_data'].format(input_message=safe_message)
            event.data = bytes(command, 'utf-8')
            event.message_id = handler_message_id

            await usecase(event)

    async def add_task(self, uc_class, event, handler_name: str, message_id: int, call_me_data: str) -> None:
        await self.__bind(uc_class)
        chat_id = event.chat_id
        self.tasks[chat_id] = {'name': handler_name, 'message_id': message_id, 'call_me_data': call_me_data}

    async def delete_task(self, chat_id) -> None:
        self.tasks.pop(chat_id, None)


async def check_max_length_search(event, send_manager) -> str | None:
    """Проверяет длину поискового запроса в бот"""
    message = event.message.text
    if len(message) > MAX_SEARCH_LENGTH:
        await send_manager.exec(
            event.client.send_message,
            entity=event.chat.id,
            message=f'Слишком длинный запрос, поcтарайтесь уложиться в {MAX_SEARCH_LENGTH} символов 👀',
        )
    else:
        return message
