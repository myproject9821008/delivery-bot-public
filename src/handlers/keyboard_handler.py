from telethon import custom
from telethon.events import NewMessage

from src.render.main_menu_render import MainMenuRender
from handlers.base_handlers import BaseKeyboardHandler


class KeyboardHandler(BaseKeyboardHandler):
    _handler_keys = {
        '/start': 'restart_main_menu_handler',
        '🌐 Главное меню': 'restart_main_menu_handler'
    }

    async def restart_main_menu_handler(self, event: NewMessage.Event) -> None:
        await self.client.send_message(
            message='Hello user!',
            entity=event.chat.id,
            buttons=[[
                custom.Button.text('🌐 Главное меню', resize=True),
                custom.Button.text('🧑‍🚒 Поддержка', resize=True)
            ]]
        )
        if not await self.active_subscription(event=event, start=True):
            return

        render_init = MainMenuRender(self.cache_manager)
        render = await render_init.execute()
        await self.send_manager.exec(
            event.client.send_message,
            message=render.text,
            file=render.file,
            entity=event.chat.id,
            buttons=render.buttons
        )

        await self.listener.delete_task(event.chat_id)
