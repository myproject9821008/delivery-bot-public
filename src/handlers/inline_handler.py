from telethon.events import CallbackQuery

from handlers.base_handlers import BaseInlineHandler
from render.channel_render import ChannelRender, ChanConfRender
from render.chat_render import ChatRender, ChatConfRender, ChMembersRender
from render.main_menu_render import MainMenuRender
from render.member_render import MemberRender, MemberConfRender, MemberChRender
from schemas.command_schemas import Action
from schemas.handler_schemas import (
    ChatsSchema, ChatConfSchema, ChMembersSchema,
    ChannelsSchema, ChanConfSchema,
    MembersSchema, MemberConfSchema, MemberChSchema,
)


class InlineHandler(BaseInlineHandler):
    async def empty_handler(self, event: CallbackQuery.Event) -> None:
        await self.send_manager.exec(event.answer)

    async def subscription_handler(self, event: CallbackQuery.Event) -> None:
        if not await self.active_subscription(event=event):
            await self.send_manager.exec(event.answer, 'Продолжить можно только с подпиской', alert=True)
            return

        await self.main_menu_handler(event)

    async def main_menu_handler(self, event: CallbackQuery.Event) -> None:
        if not await self.active_subscription(event=event):
            return

        render_init = MainMenuRender(self.cache_manager)
        render = await render_init.execute()
        await self.send_manager.exec(
            self.client.edit_message,
            entity=event.sender_id,
            message=event.message_id,
            text=render.text,
            file=render.file,
            buttons=render.buttons
        )
        await self.listener.delete_task(event.chat_id)

    async def channels_handler(self, event: CallbackQuery.Event) -> None:
        if not await self.active_subscription(event=event):
            return

        frame: ChannelsSchema = await self.get_inline_params(event)

        if frame.action == Action.EMPTY:
            channels = await self.data.get_channels_listing(frame)
            render_init = ChannelRender(frame, channels, self.cache_manager)
            render = await render_init.execute()
            await self.send_manager.exec(
                self.client.edit_message,
                entity=frame.sender,
                message=frame.message_id,
                text=render.text,
                file=render.file,
                buttons=render.buttons
            )
            return

        if frame.action == Action.SEARCH:
            await self.send_manager.exec(event.answer)
            await self.send_manager.exec(
                self.client.send_message,
                entity=event.chat.id,
                message='⬇ Введите имя, которое хотите найти, или его часть ⬇',
            )
            to_insert_mark = '{input_message}'
            call_me_data = f'{frame.name}##{Action.SEARCH_RESULT}#{to_insert_mark}#'
            await self.listener.add_task(self, event, str(frame.name), frame.message_id, call_me_data)
            return

        if frame.action == Action.SEARCH_RESULT:
            chats = await self.data.get_channels_listing(frame)
            render_init = ChannelRender(frame, chats, self.cache_manager)
            render = await render_init.execute()
            await self.send_manager.exec(
                self.client.send_message,
                entity=frame.chat,
                message=render.text,
                buttons=render.buttons,
                file=render.file
            )
            await self.listener.delete_task(event.chat_id)
            return

        chats = await self.data.change_channels_listing(frame)
        render_init = ChannelRender(frame, chats, self.cache_manager)
        render = await render_init.execute()
        await self.send_manager.exec(
            self.client.edit_message,
            entity=frame.sender,
            message=frame.message_id,
            text=render.text,
            file=render.file,
            buttons=render.buttons
        )

    async def chan_conf_handler(self, event: CallbackQuery.Event) -> None:
        if not await self.active_subscription(event=event):
            return

        frame: ChanConfSchema = await self.get_inline_params(event)
        channel = None

        if frame.action == Action.EMPTY:
            channel = await self.data.get_one_channel(frame)
        if frame.action in Action:
            channel = await self.data.change_one_channel(frame)

        render_init = ChanConfRender(frame, channel, self.cache_manager)
        render = await render_init.execute()
        await self.send_manager.exec(
            self.client.edit_message,
            entity=frame.sender,
            message=frame.message_id,
            text=render.text,
            file=render.file,
            buttons=render.buttons
        )

    async def chats_handler(self, event: CallbackQuery.Event) -> None:
        if not await self.active_subscription(event=event):
            return

        frame: ChatsSchema = await self.get_inline_params(event)

        if frame.action == Action.EMPTY:
            chats = await self.data.get_chats_listing(frame)
            render_init = ChatRender(frame, chats, self.cache_manager)
            render = await render_init.execute()
            await self.send_manager.exec(
                self.client.edit_message,
                entity=frame.sender,
                message=frame.message_id,
                text=render.text,
                file=render.file,
                buttons=render.buttons
            )
            return

        if frame.action == Action.SEARCH:
            await self.send_manager.exec(event.answer)
            await self.send_manager.exec(
                self.client.send_message,
                entity=event.chat.id,
                message='⬇ Введите имя, которое хотите найти, или его часть ⬇'
            )
            to_insert_mark = '{input_message}'
            call_me_data = f'{frame.name}##{Action.SEARCH_RESULT}#{to_insert_mark}#'
            await self.listener.add_task(self, event, str(frame.name), frame.message_id, call_me_data)
            return

        if frame.action == Action.SEARCH_RESULT:
            chats = await self.data.get_chats_listing(frame)
            render_init = ChatRender(frame, chats, self.cache_manager)
            render = await render_init.execute()
            await self.send_manager.exec(
                self.client.send_message,
                entity=frame.chat,
                message=render.text,
                buttons=render.buttons,
                file=render.file
            )
            await self.listener.delete_task(event.chat_id)
            return

        chats = await self.data.change_chats_listing(frame)
        render_init = ChatRender(frame, chats, self.cache_manager)
        render = await render_init.execute()
        await self.send_manager.exec(
            self.client.edit_message,
            entity=frame.sender,
            message=frame.message_id,
            text=render.text,
            file=render.file,
            buttons=render.buttons
        )

    async def chat_conf_handler(self, event: CallbackQuery.Event) -> None:
        if not await self.active_subscription(event=event):
            return

        frame: ChatConfSchema = await self.get_inline_params(event)
        chat = None

        if frame.action == Action.EMPTY:
            chat = await self.data.get_one_chat(frame)
        if frame.action in Action:
            chat = await self.data.change_one_chat(frame)

        render_init = ChatConfRender(frame, chat, self.cache_manager)
        render = await render_init.execute()
        await self.send_manager.exec(
            self.client.edit_message,
            entity=frame.sender,
            message=frame.message_id,
            text=render.text,
            file=render.file,
            buttons=render.buttons
        )

    async def ch_members_handler(self, event: CallbackQuery.Event) -> None:
        if not await self.active_subscription(event=event):
            return
        frame: ChMembersSchema = await self.get_inline_params(event)

        if frame.action == Action.EMPTY:
            chats = await self.data.get_chat_members_listing(frame)
            render_init = ChMembersRender(frame, chats, self.cache_manager)
            render = await render_init.execute()
            await self.send_manager.exec(
                self.client.edit_message,
                entity=frame.sender,
                message=frame.message_id,
                text=render.text,
                file=render.file,
                buttons=render.buttons
            )
            return

        if frame.action == Action.SWITCH_ALL:
            chats = await self.data.off_all_chat_members(frame)
            render_init = ChMembersRender(frame, chats, self.cache_manager)
            render = await render_init.execute()
            await self.send_manager.exec(
                self.client.edit_message,
                entity=frame.sender,
                message=frame.message_id,
                text=render.text,
                file=render.file,
                buttons=render.buttons
            )
            return

        if frame.action == Action.SEARCH:
            await self.send_manager.exec(event.answer)
            await self.send_manager.exec(
                self.client.send_message,
                entity=event.chat.id,
                message='⬇ Введите имя, которое хотите найти, или его часть ⬇'
            )
            to_insert_mark = '{input_message}'
            call_me_data = f'{frame.name}##{Action.SEARCH_RESULT}#{to_insert_mark}#{frame.payload}'
            await self.listener.add_task(self, event, str(frame.name), frame.message_id, call_me_data)
            return

        if frame.action == Action.SEARCH_RESULT:
            chats = await self.data.get_chat_members_listing(frame)
            render_init = ChMembersRender(frame, chats, self.cache_manager)
            render = await render_init.execute()
            await self.send_manager.exec(
                self.client.send_message,
                entity=frame.chat,
                message=render.text,
                buttons=render.buttons,
                file=render.file
            )
            await self.listener.delete_task(event.chat_id)
            return

        chats = await self.data.change_chat_members_listing(frame)
        render_init = ChMembersRender(frame, chats, self.cache_manager)
        render = await render_init.execute()
        await self.send_manager.exec(
            self.client.edit_message,
            entity=frame.sender,
            message=frame.message_id,
            text=render.text,
            file=render.file,
            buttons=render.buttons
        )

    async def members_handler(self, event: CallbackQuery.Event) -> None:
        if not await self.active_subscription(event=event):
            return
        frame: MembersSchema = await self.get_inline_params(event)

        if frame.action == Action.EMPTY:
            chats = await self.data.get_members_listing(frame)
            render_init = MemberRender(frame, chats, self.cache_manager)
            render = await render_init.execute()
            await self.send_manager.exec(
                self.client.edit_message,
                entity=frame.sender,
                message=frame.message_id,
                text=render.text,
                file=render.file,
                buttons=render.buttons
            )
            return

        if frame.action == Action.SEARCH:
            await self.send_manager.exec(event.answer)
            await self.send_manager.exec(
                self.client.send_message,
                entity=event.chat.id,
                message='⬇ Введите имя, которое хотите найти, или его часть ⬇'
            )
            to_insert_mark = '{input_message}'
            call_me_data = f'{frame.name}##{Action.SEARCH_RESULT}#{to_insert_mark}#'
            await self.listener.add_task(self, event, str(frame.name), frame.message_id, call_me_data)
            return

        if frame.action == Action.SEARCH_RESULT:
            chats = await self.data.get_members_listing(frame)
            render_init = MemberRender(frame, chats, self.cache_manager)
            render = await render_init.execute()
            await self.send_manager.exec(
                self.client.send_message,
                entity=frame.chat,
                message=render.text,
                buttons=render.buttons,
                file=render.file
            )
            await self.listener.delete_task(event.chat_id)
            return

        chats = await self.data.change_members_listing(frame)
        render_init = MemberRender(frame, chats, self.cache_manager)
        render = await render_init.execute()
        await self.send_manager.exec(
            self.client.edit_message,
            entity=frame.sender,
            message=frame.message_id,
            text=render.text,
            file=render.file,
            buttons=render.buttons
        )

    async def member_conf_handler(self, event: CallbackQuery.Event) -> None:
        if not await self.active_subscription(event=event):
            return
        frame: MemberConfSchema = await self.get_inline_params(event)
        chat = None

        if frame.action == Action.EMPTY:
            chat = await self.data.get_one_member(frame)
        if frame.action in Action:
            chat = await self.data.change_one_member(frame)

        render_init = MemberConfRender(frame, chat, self.cache_manager)
        render = await render_init.execute()
        await self.send_manager.exec(
            self.client.edit_message,
            entity=frame.sender,
            message=frame.message_id,
            text=render.text,
            file=render.file,
            buttons=render.buttons
        )

    async def member_ch_handler(self, event: CallbackQuery.Event) -> None:
        if not await self.active_subscription(event=event):
            return
        frame: MemberChSchema = await self.get_inline_params(event)

        if frame.action == Action.EMPTY:
            chats = await self.data.get_member_chats(frame)
            render_init = MemberChRender(frame, chats, self.cache_manager)
            render = await render_init.execute()
            await self.send_manager.exec(
                self.client.edit_message,
                entity=frame.sender,
                message=frame.message_id,
                text=render.text,
                file=render.file,
                buttons=render.buttons
            )
            return

        if frame.action == Action.SWITCH_ALL:
            chats = await self.data.off_all_member_chats(frame)
            render_init = MemberChRender(frame, chats, self.cache_manager)
            render = await render_init.execute()
            await self.send_manager.exec(
                self.client.edit_message,
                entity=frame.sender,
                message=frame.message_id,
                text=render.text,
                file=render.file,
                buttons=render.buttons
            )
            return

        if frame.action == Action.SEARCH:
            await self.send_manager.exec(event.answer)
            await self.send_manager.exec(
                self.client.send_message,
                entity=event.chat.id,
                message='⬇ Введите имя, которое хотите найти, или его часть ⬇'
            )
            to_insert_mark = '{input_message}'
            call_me_data = f'{frame.name}##{Action.SEARCH_RESULT}#{to_insert_mark}#{frame.payload}'
            await self.listener.add_task(self, event, str(frame.name), frame.message_id, call_me_data)
            return

        if frame.action == Action.SEARCH_RESULT:
            chats = await self.data.get_member_chats(frame)
            render_init = MemberChRender(frame, chats, self.cache_manager)
            render = await render_init.execute()
            await self.send_manager.exec(
                self.client.send_message,
                entity=frame.chat,
                message=render.text,
                buttons=render.buttons,
                file=render.file
            )
            await self.listener.delete_task(event.chat_id)
            return

        chats = await self.data.change_member_chats_listing(frame)
        render_init = MemberChRender(frame, chats, self.cache_manager)
        render = await render_init.execute()
        await self.send_manager.exec(
            self.client.edit_message,
            entity=frame.sender,
            message=frame.message_id,
            text=render.text,
            file=render.file,
            buttons=render.buttons
        )

    @staticmethod
    async def alert_handler(event: CallbackQuery.Event) -> None:
        await event.answer('Исчезающее оповещение')

    @staticmethod
    async def alert_dialog_handler(event: CallbackQuery.Event) -> None:
        await event.answer('Диалоговое окно', alert=True)
