from abc import ABC

from pydantic import ConfigDict, BaseModel
from telethon import events, TelegramClient
from telethon.events import NewMessage, CallbackQuery

from interfaces.cache_manager_interface import CacheManagerInterface
from interfaces.data_adapter_interface import DataAdapterInterface
from interfaces.listener_manager_interface import ListenersManagerInterface
from interfaces.send_manager_interface import SendManagerInterface
from interfaces.ui_handler_interface import UIHandlerInterface, UIInlineHandlerInterface, UIKeyboardHandlerInterface
from render.subscribe_render import SubscribeRender
from schemas.handler_schemas import FrameCommonType
from shared.common_utils import place_borders
from src.config import ITEMS_PER_PAGE
from src.shared.exceptions import HandlerBindException
from src.schemas.schema_builder import SchemaBuilder


class HandlerParams(BaseModel):
    model_config = ConfigDict(arbitrary_types_allowed=True)

    client: TelegramClient
    listener: ListenersManagerInterface
    data_adapter: DataAdapterInterface
    cache_manager: CacheManagerInterface
    send_manager: SendManagerInterface


class BaseHandler(UIHandlerInterface, ABC):
    def __init__(self, params: HandlerParams):
        if not isinstance(params, HandlerParams):
            raise ValueError(f'expected HandlerParams instance, but got {type(params)}: {params}')
        self.__params = params

        self.__handlers = self.__handlers_list()
        self.__data = params.data_adapter

    @property
    def client(self) -> TelegramClient:
        return self.__params.client

    @property
    def listener(self) -> ListenersManagerInterface:
        return self.__params.listener

    @property
    def cache_manager(self) -> CacheManagerInterface:
        return self.__params.cache_manager

    @property
    def send_manager(self) -> SendManagerInterface:
        return self.__params.send_manager

    @property
    def data(self) -> DataAdapterInterface:
        return self.__data

    @property
    def handlers(self) -> list:
        return self.__handlers

    def __handlers_list(self) -> list:
        result = []
        for func in dir(self.__class__):
            if func.endswith('_handler') and callable(getattr(self, func)):
                result.append(func)
        return result

    async def active_subscription(self, event, start: bool = False) -> bool:
        """Common handler for checking user subscription"""
        sub = await self.data.check_subscribe(event.sender.id)
        if sub:
            return True
        render_init = SubscribeRender(event.client.cache_manager)
        render = await render_init.execute()
        if start:
            await self.send_manager.exec(
                event.client.send_message,
                entity=event.chat.id,
                message=render.text,
                file=render.file,
                buttons=render.buttons,
            )
        else:
            await self.send_manager.exec(
                event.client.edit_message,
                entity=event.sender_id,
                message=event.message_id,
                text=render.text,
                file=render.file,
                buttons=render.buttons,
            )
        return False


class BaseInlineHandler(BaseHandler, UIInlineHandlerInterface):
    async def bind(self):
        for handler_name in self.handlers:
            event_code = '_'.join(handler_name.split('_')[:-1])
            handler = getattr(self, handler_name)
            self.client.add_event_handler(handler, events.CallbackQuery(pattern=f'^{event_code}$|^{event_code}#.+'))

    @staticmethod
    async def get_inline_params(event: CallbackQuery.Event) -> FrameCommonType:
        raw_str = str(event.data, 'utf-8')
        groups = raw_str.split('/')
        params_result = []
        for group in groups:
            params = group.split('#')
            clearing_service_symbols = list(map(place_borders, params))
            params_result.append(clearing_service_symbols)

        index = raw_str.find('/')
        payload = f'/{raw_str[index + 1:]}' if '/' in raw_str else ''

        builder = SchemaBuilder(params=params_result, payload=payload, event=event, count=ITEMS_PER_PAGE)
        result = await builder.execute()
        return result


class BaseKeyboardHandler(BaseHandler, UIKeyboardHandlerInterface):
    _handler_keys = None

    async def bind(self):
        for pattern, handler_name in self._handler_keys.items():
            if handler_name not in self.handlers:
                raise HandlerBindException(f'Handler "{handler_name}" with pattern "{pattern}" does not exist')
            handler = getattr(self, handler_name)
            self.client.add_event_handler(handler, NewMessage(pattern=f'^{pattern}$'))
